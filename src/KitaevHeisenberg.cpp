/*
 * KitaevHeisenberg.cpp
 *
 *  Created on: 2014骞�1���24���
 *      Author: ZhaoHuihai
 */

// C++ Standard Template Library
//	1. C library
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <cassert>
//	2. Containers
#include <vector>
//	3. Input/Output
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
//	4. Other
#include <algorithm>
#include <string>
#include <complex>
#include <utility>
//-----------------------------
// intel mkl
#include <mkl.h>

#include "TensorOperation.hpp"

#include "parameter.h"
#include "operators.h"
#include "wave_function.h"
#include "imaginary_time_evolution_honeycomb.h"
#include "KitaevHeisenberg.h"

using namespace std ;

KitaevHeisenberg::KitaevHeisenberg() {}

KitaevHeisenberg::KitaevHeisenberg(Parameter& parameter)
{
	WaveFunction wave(parameter) ;

	//-------------------------------------------
	ImaginaryTimeEvolutionHoneycomb ITE(parameter, wave) ;
	//-------------------------------------------
	if (parameter.EVC)
	{
		cout << "compute expectation value" << endl ;
		findExpectValue(parameter, wave) ;
	}
}

KitaevHeisenberg::~KitaevHeisenberg()
{

}
//==============================================================================================

void KitaevHeisenberg::findExpectValue(Parameter& parameter, WaveFunction &wave)
{
	parameter.RG_method = parameter.EVC_RG_method ;
	parameter.SRG_trunc = parameter.EVC_SRG_trunc ;
	parameter.SRG_norm  = parameter.EVC_SRG_norm  ;

	parameter.RG_bondDim = parameter.EVC_RG_bondDim ;
	parameter.RG_minVal = parameter.EVC_RG_minVal ;
	parameter.sweep_maxIt = parameter.EVC_sweep_maxIt ;
	parameter.RGsteps = parameter.EVC_RGsteps ;
	//----------------------------------------------------------------------------
	cout << "EVC_RG_method: " << parameter.RG_method << endl ;
	cout << "RG steps: " << parameter.RGsteps << endl ;
	cout << "lattice size: " << 8 * pow((double)3, parameter.RGsteps) << endl ;
	cout << "RG dimension: " << parameter.RG_bondDim << endl ;
	cout << "field z: " << parameter.field_z << endl ;
	cout << "---------------------------------------------------" << endl ;
//	exit(0) ;
	Tensor<double> energyBond(3) ;
	Tensor<double> NN_Bond(3) ;

	Tensor<double> MzSite(2) ;
	Tensor<double> MxSite(2) ;

	TensorArray<double> T(2) ;
	for (int i = 0; i < 2; i ++)
	{
		// T(0)((z0,z0'),(x0,x0'),(y0,y0')) = sum{m0}_[A(0)(z0,x0,y0,m0)*A(0)(z0',x0',y0',m0)]
		T(i) = tracePhysInd(wave.A(i)) ;
	}

	//	parameter.field_z = 0.0 ;
	Operators operators(parameter) ;
	TensorArray<double> hamiltonian = operators.createHamiltonian_Kitaev() ;
	Tensor<double> Sz = operators.createSz() ;
	Tensor<double> Sx = operators.createSx() ;
	Tensor<double> uni = operators.createUni() ; // unit operator
	Tensor<double> SzSz = operators.createSzSz() ;
	Tensor<double> SxSx = operators.createSxSx() ;
	Tensor<double> SySy = operators.createSySy() ;
	Tensor<double> Heisen = operators.createHamiltonian_Heisenberg() ;
	//------------
	// env: save all intermediate tensors
	EnvironmentHoneycomb env(parameter, T) ;
	//--------
	for ( int i = 0; i < 3; i ++ )
	{
		Tensor<double> Menv = env.compute_Menv() ; // Menv(x0,y0,x1,y1)
		energyBond(i) = env.computeBond(parameter, wave.A, T, Menv, hamiltonian(i)) ;
		cout << "energyBond( "<< i << " ): " << energyBond(i) << endl ;
		//-------------------------------------------------------------
		if ( i == 0 ) // z bond direction
		{
			MzSite(0) = env.computeSite(parameter, wave.A, T, Menv, Sz, uni) ;
			MzSite(1) = env.computeSite(parameter, wave.A, T, Menv, uni, Sz) ;

			MxSite(0) = env.computeSite(parameter, wave.A, T, Menv, Sx, uni) ;
			MxSite(1) = env.computeSite(parameter, wave.A, T, Menv, uni, Sx) ;
		}

		//-------------------------------------------------------------
		wave.rotate() ;
		env.rotate() ;
		for ( int j = 0; j < 2; j ++ )
		{
//			T(j) = T(j).permute(1, 2, 0, 3) ;
			T(j) = T(j).permute(1, 2, 0) ;
		}
	}
	//-------------------------------------
	double energy = energyBond.mean() * 1.5 ;
	double energy_bond_SD = standard_deviation(energyBond) ;
	//-------------------------------------
	cout << "----------------------------------------------------" << endl ;
	cout << "energy: " << std::setprecision(10) << energy << endl ;
	cout << "bond energy difference: " << std::setprecision(10) << energy_bond_SD << endl ;
	//************************************************************************
	cout << "====================================================" << endl ;
	cout << "MzSite(0): " << MzSite(0) << endl ;
	cout << "MzSite(1): " << MzSite(1) << endl ;

	cout << "MxSite(0): " << MxSite(0) << endl ;
	cout << "MxSite(1): " << MxSite(1) << endl ;

	double stagMz, stagMx ;
	cout << "----------------------------------------------------" << endl ;
	stagMz = fabs( MzSite(0) - MzSite(1) ) / 2.0 ;
	cout << "staggered Mz: " << stagMz << endl ;

	stagMx = fabs( MxSite(0) - MxSite(1) ) / 2.0 ;
	cout << "staggered Mx: " << stagMx << endl ;

	double Mag = computeMag(MzSite, MxSite) ;
	cout << "general magnetization: " << Mag << endl ;
}

double KitaevHeisenberg::findEnergy(Parameter& parameter, WaveFunction &wave)
{
	parameter.RG_method = parameter.EVC_RG_method ;
	parameter.SRG_trunc = parameter.EVC_SRG_trunc ;
	parameter.SRG_norm  = parameter.EVC_SRG_norm  ;

	parameter.RG_bondDim = parameter.EVC_RG_bondDim ;
	parameter.RG_minVal = parameter.EVC_RG_minVal ;
	parameter.sweep_maxIt = parameter.EVC_sweep_maxIt ;
	parameter.RGsteps = parameter.EVC_RGsteps ;
	//----------------------------------------------------------------------------
	cout << "EVC_RG_method: " << parameter.RG_method << endl ;
	cout << "RG steps: " << parameter.RGsteps << endl ;
	cout << "lattice size: " << 8 * pow((double)3, parameter.RGsteps) << endl ;
	cout << "RG dimension: " << parameter.RG_bondDim << endl ;
	cout << "field z: " << parameter.field_z << endl ;
	cout << "---------------------------------------------------" << endl ;
	Tensor<double> energyBond(3) ;

	TensorArray<double> T(2) ;
	for (int i = 0; i < 2; i ++)
	{
		// T(0)((z0,z0'),(x0,x0'),(y0,y0')) = sum{m0}_[A(0)(z0,x0,y0,m0)*A(0)(z0',x0',y0',m0)]
		T(i) = tracePhysInd(wave.A(i)) ;
	}

	//	parameter.field_z = 0.0 ;
	Operators operators(parameter) ;
	TensorArray<double> hamiltonian = operators.createHamiltonian_Kitaev() ;
	//------------
	// env: save all intermediate tensors
	EnvironmentHoneycomb env(parameter, T) ;

	//--------
	for ( int i = 0; i < 3; i ++ )
	{
		Tensor<double> Menv = env.compute_Menv() ; // Menv(x0,y0,x1,y1)
		energyBond(i) = env.computeBond(parameter, wave.A, T, Menv, hamiltonian(i)) ;
		cout << "energyBond( "<< i << " ): " << energyBond(i) << endl ;

		//-------------------------------------------------------------
		wave.rotate() ;
		env.rotate() ;
		for ( int j = 0; j < 2; j ++ )
		{
//			T(j) = T(j).permute(1, 2, 0, 3) ;
			T(j) = T(j).permute(1, 2, 0) ;
		}
	}
	//-------------------------------------
	double energy = energyBond.mean() * 1.5 ;
	//-------------------------------------
	cout << "energy: " << std::setprecision(10) << energy << endl ;

	return energy ;
}

double KitaevHeisenberg::standard_deviation(Tensor<double>& bond)
{
	double a = bond.mean() ;

	double SD = 0 ;

	for ( int i = 0; i < bond.numel(); i ++ )
	{
		SD += (bond.at(i) - a) * (bond.at(i) - a) ;
	}

	return sqrt(SD / bond.numel()) ;
}

double KitaevHeisenberg::computeMag(Tensor<double> MzSite, Tensor<double> MxSite)
{
	double Mag = 0.0 ;
	for (int i = 0 ; i < 2; i ++)
	{
		Mag += 0.5 * ( MzSite(i) * MzSite(i) + MxSite(i) * MxSite(i) ) ;
	}
	return sqrt(Mag) ;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// T((z0,z0'),(x0,x0'),(y0,y0')) = sum{m0}_[A(z0,x0,y0,m0)*A(z0',x0',y0',m0)]
Tensor<double> KitaevHeisenberg::tracePhysInd(Tensor<double> &A)
{
	int dz = A.dimension(0) ;
	int dx = A.dimension(1) ;
	int dy = A.dimension(2) ;

	// T(z,x,y,z',x',y') = sum{m}_[A(z,x,y,m)*A(z',x',y',m)]
	Tensor<double> T = contractTensors(A, 3, A, 3) ;

	// T(z,x,y,z',x',y') -> T(z,z',x,x',y,y')
	T = T.permute(0, 3, 1, 4, 2, 5) ;

	// T(z,z',x,x',y,y') -> T((z,z'),(x,x'),(y,y'))
	T = T.reshape(dz * dz, dx * dx, dy * dy) ;

	return T ;
}

//---------------------------------------------------------------------------------------------
