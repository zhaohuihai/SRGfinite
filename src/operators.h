/*
 * operators.h
 *
 *  Created on: 2013-8-14
 *      Author: ZhaoHuihai
 */

#ifndef OPERATORS_H_
#define OPERATORS_H_

#include "TensorOperation.hpp"

#include "parameter.h"

class Operators
{
private:

	// model:
	// 1. Heisenberg model
	// 2. transverse field Ising
	// 3. classical Ising model
	//
	int model ;
	// site dimension
	int siteDim ;

	double J ;
	//---------------------------
	// Kitaev term coupling
	double Kx, Ky, Kz ;
	//*********************
	// longitudinal field
	double field_z ;

	double delta(int a, int b) ;
public:
	// constructor
	Operators() ;
	Operators(Parameter parameter) ;
	// destructor
	~Operators() ;
	//----------------------------------------------
	//----------------------------------------------
	Tensor<double> createHamiltonian_Heisenberg() ;

	Tensor<double> createUni() ;
	Tensor<double> createSz() ;
	Tensor<double> createSx() ;

	Tensor<double> createStagSz() ;
	//----------------------------------------------

	Tensor<double> createSigma_z() ;
	Tensor<double> createSigma_x() ;
	//-----------------------------------------------------
	TensorArray<double> createHamiltonian_Kitaev() ;

	Tensor<double> createSzSz() ;
	Tensor<double> createSxSx() ;
	Tensor<double> createSySy() ;

	// h12 = - 2*Kq*S1q*S2q - field*(h1 + h2)
	Tensor<double> addField(Tensor<double> SqSq, Tensor<double> h1, Tensor<double> h2) ;

};



#endif /* OPERATORS_H_ */
