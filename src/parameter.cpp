/*
 * parameter.cpp
 *
 *  Created on: 2011-6-9
 *      Author: zhaohuihai
 */

// C++ Standard Template Library
//	1. C library
#include <stdlib.h>
#include <cmath>
#include <iostream>

#include "parameter.h"

//using namespace std ;

Parameter::Parameter()
{
	defineModelParameter() ;
	defineTNSparameter() ;
	defineITEparameter() ;
	defineEVCparameter() ;
	defineRGparameter() ;
	defineDebugParameter() ;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
//============================================================================
void Parameter::defineModelParameter()
{
	/// define model parameters
	// model:
	// 1. AF Heisenberg model
	// 3. classical Ising model
	// 4. Kitaev model
	model = 3 ;
	//******************************
	// dimension of physical index
	siteDim = 2 ;
	// AFM : J = 1; FM: J = -1
	J = 1.0 ;
	//*********************
	// longitudinal field
	field_z = 0.0 ;
	//*********************
	// lattice size: 8*3^RGsteps
	RGsteps = 1 ;

	if (model == 3)
	{
		Ising() ;
	}
	else if (model == 4)
	{
		Kitaev() ;
	}
}

void Parameter::Kitaev()
{
	// dimension of physical index
	siteDim = 2 ;
	// Kitaev term coupling
	Kx = 1.0 ;
	Ky = 1.0 ;
	Kz = 1.0 ;
	// z field
	field_z = 0.0 ;
	//*********************
	// lattice size: 8*3^RGsteps
	RGsteps = 1 ;
}

void Parameter::Ising()
{
	// AFM : J = 1; FM: J = -1
	J = - 1 ;
	// lattice size: 4*3^RGsteps
	RGsteps = 30 ; // must be even number
	if ( RGsteps % 2 != 0 )
	{
		std::cout << "RGsteps for Ising square lattice must be even number!" << std::endl ;
		exit(0) ;
	}

	// temperature
	Ising_temp = 2.0/log(1.0 + sqrt(2.0)) ;
//	Ising_temp = 2.1 ;
	Ising_beginTemp = 2.0/log(1.0 + sqrt(2.0)) ;
	Ising_endTemp = Ising_beginTemp ;
	Ising_tempStep = 2.0 ;

	Ising_RG_method = 1 ; // 1: TRG, 2: SRG with sweep, 3: infinite SRG
	// 1: old SRG(decompose environment)
	// 2: symmetrize bond density matrix
	// 3: biorthogonal
	// 4: variation
	// 5: isometry variation
	Ising_SRG_trunc = 1;
	Ising_SRG_norm = false ;
	Ising_rho_norm = false ;

	Ising_SRG_vari_maxIt = 400 ;
	Ising_SRG_vari_tol = 1e-12 ;

	Ising_RG_bondDim  = 4 ;
	Ising_RG_minVal   = 1e-100 ;
	Ising_sweep_maxIt = 0 ;
}

void Parameter::defineTNSparameter()
{
	/// define tensor network state(TNS) parameters
	// number of sites in one unit cell
	TNS_subLatNo  = 2 ;
	//Bond dimension of wave function
	TNS_bondDim = 4 ;
	// if initial tensors are same
	TNS_sameInitial = false ;
	// a: arithmetic Sequence
	// r: rand
	TNS_entryType = 'r' ;
	//
	TNS_complex = false ;
}

void Parameter::defineITEparameter()
{
	/// define imaginary time evolution parameters
	// load saved wave function or not
	ITE_loadPreviousWave = false ;
	// apply imaginary time evolution(ITE) or not
	ITE = true ;
	// ITE method
	// 1: simple update
	ITE_simpleUpdate = true ;
	ITE_fullupdate = false ;
	//-------------------------------
	// cut off of min value
	ITE_simUpd_minVal = 1e-14 ;
	//-------------------------------
	// apply polarization or not
	ITE_polarization = false ;

	// Neel field
	ITE_polarField_z = 1.0 ;

	ITE_tauInitial_polar = 1 ;
	ITE_tauFinal_polar = 1e-1 ;
	ITE_tauReduceStep_polar = 100 ;
	ITE_tauFinalStep_polar = 10 ;
	//-------------------------------
	// tau of simple update
	ITE_tauInitial_simple = 5e-1 ;
	ITE_tauFinal_simple = 1e-2 ;
	ITE_tauReduceStep_simple = 1000 ; // tau reduce steps with tau reduced by factor of 10
	ITE_tauFinalStep_simple = 200 ;
	//--------------------------------
	// tau of variation: reduce tau exponentially
	ITE_tauInitial_full = 3e-1 ;
	ITE_tauFinal_full = 1e-2 ;
	ITE_tauReduceStep_full = 100 ;
	ITE_tauFinalStep_full = 19 ;
	// calculate energy after n steps of full update
	ITE_step_perCalEng = 20 ;
	// 'S': simple update as initial
	// 'B': bond density matrix as initial
	ITE_fullInitial = 'S' ;
	// RG method
	// 1: TRG
	// 2: SRG with sweep
	ITE_RG_method = 1 ;
	// 1: old SRG(decompose environment)
	// 2: symmetrize bond density matrix
	// 3: biorthogonal
	ITE_SRG_trunc = 1    ;
	ITE_SRG_norm  = false ;
	// when contracting environment tensor network by RG,
	// truncation bond dimension is:
	ITE_RG_bondDim = 36 ;
	ITE_RG_minVal = 1e-100 ;
	ITE_sweep_maxIt = 0 ;

	// Optimization by solving linear equations
	ITE_maxLinEqStep = 20 ;
	ITE_newProjRatio = 1 ;
	ITE_tol_LinEq = 1e-12 ;
	ITE_LinEq_minSing = 1e-10 ; // min singular value in computation of matrix inverse
	ITE_minEig = 0.0 ; // positive approximant
	ITE_gaugeFix = true ;
	ITE_preCond = true ;
}

void Parameter::defineEVCparameter()
{
	/// define expectation value computation(EVC) parameters
	// apply expectation value computation(EVC) or not
	EVC = true ;
	// EVC method
	// 1: TRG
	// 2: SRG with sweep
	// 3: infinite SRG
	EVC_RG_method = 2 ;
	// 1: old SRG(decompose environment)
	// 2: symmetrize bond density matrix
	// 3: biorthogonal
	// 4: variation
	// 5: isometry variation
	EVC_SRG_trunc = 1 ;
	EVC_SRG_norm = false ;
	EVC_rho_norm = false ;

	EVC_RG_bondDim = 16 ;
	EVC_RG_minVal = 1e-100 ;
	EVC_sweep_maxIt = 0 ;
	//-------------------------------------
	EVC_RGsteps = RGsteps ;
}

void Parameter::defineRGparameter()
{
	// define renormalization group parameter
	// RG method
	// 1: TRG
	// 2: SRG with sweep
	// 3: infinite SRG
	RG_method = 2 ;
	// 1: old SRG
	// 2: symmetrize bond density matrix
	// 3: biorthogonal
	// 4: variation
	// 5: isometry variation
	SRG_trunc = 1 ;
	SRG_norm = false ;
	rho_norm = true ;

	SRG_vari_maxIt = 200 ;
	SRG_vari_tol = 1e-12 ;
	// truncation bond dimension
	RG_bondDim = 10 ;
	//-----------------------------------------
	// cut off of min value
	RG_minVal = 1e-100 ;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// define canonical parameter
	sweep_maxIt = 0 ;
	sweep_tol = 1e-10 ;
}

void Parameter::defineDebugParameter()
{
	/// define debugging parameters
	// display intermediate result or not
	disp = false ;
}

Parameter::~Parameter() { }
