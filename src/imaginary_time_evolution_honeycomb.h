/*
 * imaginary_time_evolution_honeycomb.h
 *
 *  Created on: 2014-1-28
 *      Author: ZhaoHuihai
 */

#ifndef IMAGINARY_TIME_EVOLUTION_HONEYCOMB_H_
#define IMAGINARY_TIME_EVOLUTION_HONEYCOMB_H_

//#include "TensorOperation/TensorOperation.hpp"

#include "parameter.h"
#include "environment_honeycomb.h"
#include "wave_function.h"

class ImaginaryTimeEvolutionHoneycomb
{
private:
	double minEig ;
public:
	// constructor
	ImaginaryTimeEvolutionHoneycomb() ;
	ImaginaryTimeEvolutionHoneycomb(Parameter parameter, WaveFunction &wave) ;
	// destructor
	~ImaginaryTimeEvolutionHoneycomb() ;
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// 1: simple update
	// 2: full update with variation
	int method ;

	bool gaugeFix ;
	bool preCond ;

	bool disp ;

	// projectors
	TensorArray<double> rightProjectors ;
	TensorArray<double> leftProjectors ;

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	void initializeProjector(Parameter &parameter) ;

	void findGroundStateWave(Parameter &parameter, WaveFunction &wave) ;

	void polarizeWave(Parameter parameter, WaveFunction &wave) ;

	void applyWaveProjection_simple(Parameter parameter, WaveFunction &wave) ;

	TensorArray<double> createHamiltonian(Parameter &parameter) ;

	void applyFirstTrotter_simple(Parameter parameter, TensorArray<double> hamiltonian,
			double tau, WaveFunction &wave, double &truncationError) ;
	//******************************
	void simpleUpdate(Parameter parameter, Tensor<double> hamiltonian, double tau,
			WaveFunction &wave, double &truncErr, Tensor<double> &PR, Tensor<double> &PL) ;
	//*********************************
	TensorArray<double> createProjectionOperator(Parameter parameter,
			Tensor<double> hamiltonian, double tau) ;
	//*********************************
	void computeProjection(TensorArray<double> PO,
			Tensor<double> A1, Tensor<double> A2, Tensor<double>& T1, Tensor<double>& T2) ;
	//*******************************
	Tensor<double> computeT_sqrtLxLy(Tensor<double> T, Tensor<double> Lx, Tensor<double> Ly) ;
	Tensor<double> computeT_sqrtLzLy(Tensor<double> T, Tensor<double> Lz, Tensor<double> Ly) ;
	Tensor<double> computeT_sqrtLzLx(Tensor<double> T, Tensor<double> Lz, Tensor<double> Lx) ;
	//*******************************

	void truncateSVD(Parameter &parameter, Tensor<double>& U,
			Tensor<double>& S, Tensor<double>& V, double& truncationError) ;
	void truncateSVD(Parameter &parameter, Tensor<double>& U,
			Tensor<double>& S, Tensor<double>& V) ;
	void truncateSVD(const int Dcut, Tensor<double>& U,
			Tensor<double>& S, Tensor<double>& V) ;
	int getDcut(Parameter &parameter, Tensor<double>& S) ;

	Tensor<double> createProjector(Tensor<double> R2, Tensor<double> V, Tensor<double> S) ;
	//-------------------------------------------------------------------------
	void applyWaveProjection_full(Parameter parameter, WaveFunction &wave) ;
	void applyFirstTrotter_full(Parameter parameter, TensorArray<double> hamiltonian,
			double tau, WaveFunction &wave, double &energy) ;
	void applyFirstTrotter_SRG(Parameter parameter, TensorArray<double> hamiltonian,
				double tau, WaveFunction &wave, double &energy) ;
	//**********************
	void fullUpdate(Parameter parameter, Tensor<double> hamiltonian, double tau,
			WaveFunction &wave, double &energyBond, Tensor<double> &PR, Tensor<double> &PL) ;

	void variation(Parameter parameter, WaveFunction &wave, Tensor<double> H,
			double &energyBond, Tensor<double> &Ap0, Tensor<double> &Ap1,
			Tensor<double> &PR, Tensor<double> &PL) ;
	//*************************************************************************
	void fullUpdate_GauFix(Parameter parameter, Tensor<double> hamiltonian,
			double tau, WaveFunction &wave, double &energyBond) ;
	void createReduTen(Tensor<double> A, Tensor<double>& X, Tensor<double>& R) ;
	void createProjectionOperator(Parameter parameter, Tensor<double> hamiltonian,
			double tau, Tensor<double>& PO) ;
	void initializeRL(Tensor<double>& aR, Tensor<double>& bL,
			Tensor<double>& aR0, Tensor<double>& bL0, Tensor<double>& PO,
			const int Dcut) ;
	void variation_GauFix(Parameter parameter, WaveFunction &wave, Tensor<double> H,
			double &energyBond, Tensor<double> &X, Tensor<double> &Y,
			Tensor<double> &aR, Tensor<double> &bL,
			Tensor<double> &aR0, Tensor<double> &bL0) ;
	Tensor<double> computeNormTensor(Tensor<double> &Menv,
			Tensor<double> &X, Tensor<double> &Y) ;
	void gauFix(Tensor<double>& NLR, Tensor<double>& X, Tensor<double>& Y,
				      Tensor<double> &aR, Tensor<double> &bL,
						  Tensor<double> &aR0, Tensor<double> &bL0) ;
	void positiveApprox(Tensor<double>& NLR) ;
	void truncateSmallEig(Tensor<double>& W, Tensor<double>& Sigma) ;
	void optimizeRL(Parameter parameter, Tensor<double>& NLR,
			Tensor<double>& aR, Tensor<double>& bL, Tensor<double>& aR0, Tensor<double>& bL0) ;
	double optimize_aR(Parameter parameter, Tensor<double>& NLR,
			Tensor<double>& aR, Tensor<double>& bL, Tensor<double>& aR0, Tensor<double>& bL0) ;
	double optimize_bL(Parameter parameter, Tensor<double>& NLR,
			Tensor<double>& aR, Tensor<double>& bL, Tensor<double>& aR0, Tensor<double>& bL0) ;
	//*************************************************************************
	void SRGfullUpdate(Parameter parameter, Tensor<double> hamiltonian, double tau,
			WaveFunction &wave, double &energyBond, Tensor<double> &PR, Tensor<double> &PL) ;
	//*************************************************************************

	Tensor<double> tracePhysInd(Tensor<double> &A) ;
	//*************************************************************************
	void decomposeOperator(Parameter& parameter,
			Tensor<double>& H, Tensor<double>& U, Tensor<double>& V) ;
	void truncateSmall(Tensor<double>& U, Tensor<double>& S, Tensor<double>& V) ;
	//************************************************************************
	Tensor<double> createTh_z(Tensor<double> &A, Tensor<double> U) ;
	Tensor<double> createTh_x(Tensor<double> &A, Tensor<double> U) ;
	Tensor<double> createTh_y(Tensor<double> &A, Tensor<double> U) ;
	//*************************************************************************
	Tensor<double> contractEnv(Tensor<double> &Mz_01, Tensor<double> &Ap0, Tensor<double> &Ap1) ;
	//*************************************************************************
	void optimizeProjectors(Parameter parameter, Tensor<double> &Me,
			Tensor<double> &PR, Tensor<double> &PL) ;

	Tensor<double> optimizePR(Parameter parameter, Tensor<double> &Me, Tensor<double> &PL) ;
	Tensor<double> optimizePL(Parameter parameter, Tensor<double> &Me, Tensor<double> &PR) ;

	void mixProjectors(double newProjRatio, Tensor<double> &PR, Tensor<double> PR0) ;

	double getDiffAsVector(Tensor<double> PR, Tensor<double> PR0) ;

	Tensor<double> computeN(Tensor<double> &Me) ;

	Tensor<double> pinv(Tensor<double> &A, double tol) ;
	int getCut(Tensor<double> &S, double tol) ;
	//************************************************
	void decomposeBonDenMat(Parameter parameter, Tensor<double> &N,
			Tensor<double> &PR, Tensor<double> &PL) ;
	void truncateDenMat(Tensor<int> &ind, Tensor<double> &Lreal, Tensor<double> &Limag,
			Tensor<double> &Vl, Tensor<double> &Vr) ;
	void SRGfindProjectors(Tensor<double> &Limag, Tensor<double> &Vl, Tensor<double> &Vr,
			Tensor<double> &PR, Tensor<double> &PL) ;
	//----------------------------------------
};



#endif /* IMAGINARY_TIME_EVOLUTION_HONEYCOMB_H_ */
