/*
 * operators.cpp
 *
 *  Created on: 2013骞�8���14���
 *      Author: ZhaoHuihai
 */

// C++ Standard Template Library
//	1. C library
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <cassert>
//	2. Containers
#include <vector>
//	3. Input/Output
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
//	4. Other
#include <algorithm>
#include <string>
#include <complex>
#include <utility>
//-----------------------------
// intel mkl
#include <mkl.h>

// my head files
#include "parameter.h"
#include "operators.h"
//=====================================================

using namespace std ;

// constructor
Operators::Operators()
{
	model = 0 ;
	siteDim = 0 ;
	J = 0.0 ;

	Kx = 1.0 ;
	Ky = 1.0 ;
	Kz = 1.0 ;

	field_z = 0.0 ;
}

Operators::Operators(Parameter parameter)
{
	model = parameter.model ;
	siteDim = parameter.siteDim ;
	J = parameter.J ;

	Kx = parameter.Kx ;
	Ky = parameter.Ky ;
	Kz = parameter.Kz ;

	field_z = parameter.field_z ;
}


// destructor
Operators::~Operators() {}

//---------------------------------------------------------------------

// AF Heisenberg model Hamiltonian
Tensor<double> Operators::createHamiltonian_Heisenberg()
{
	// total spin
	double S = ((double)siteDim - 1.0) / 2.0 ;

	Tensor<double> H(siteDim, siteDim, siteDim, siteDim) ;
	H = 0 ;
	int MM = siteDim * siteDim ;

	// H(m1, m2, n1, n2) = <m1, m2| H | n1, n2 >

	// assign values to diagonal entries
	for ( int q2 = 0; q2 < siteDim; q2 ++)
	{
		double m2 = S - (double)q2 ;
		for ( int q1 = 0; q1 < siteDim; q1 ++)
		{
			double m1 = S - (double)q1 ;
//			std::cout << "m1: " << m1 << ", m2: " << m2 << std::endl ;
			H(q1, q2, q1, q2) = H(q1, q2, q1, q2) + J * (m1 * m2) ;
		}
	}

	// assign values to off-diagonal entries
	for ( int q2 = 0; q2 < siteDim; q2 ++)
	{
		double m2 = S - (double)q2 ;
		for ( int q1 = 0; q1 < siteDim; q1 ++)
		{
			double m1 = S - (double)q1 ;
			// h1 = (1/2)((S1+)(S2-))
			double h1 = J * 0.5 * sqrt((S - m1) * (S + m1 + 1.0) * (S + m2) * (S - m2 + 1.0)) ;

			if ( h1 != 0) // <(q1-1),(q2+1)|h1|q1,q2>
			{
				int p1 = q1 - 1 ;
				int p2 = q2 + 1 ;
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h1 ;
			}
			// h2 = (1/2)((S1-)(S2+))
			double h2 = J * 0.5 * sqrt((S + m1) * (S - m1 + 1.0) * (S - m2) * (S + m2 + 1.0)) ;

			if ( h2 != 0) // <(q1+1),(q2-1)|h2|q1,q2>
			{
				int p1 = q1 + 1 ;
				int p2 = q2 - 1 ;
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h2 ;
			}
		}
	}

	H = H.reshape(MM, MM) ; // H((m1, m2), (n1, n2))
//	H.display() ;
//	exit(0) ;
	return H ;
}

// unit operator
Tensor<double> Operators::createUni()
{
	// total spin
	double S = ((double)siteDim - 1.0) / 2.0 ;

	Tensor<double> uni(siteDim, siteDim) ;
	uni = 0 ;

	for (int q1 = 0; q1 < siteDim; q1 ++)
	{
		uni(q1, q1) = 1 ;
	}
	return uni ;
}

Tensor<double> Operators::createSz()
{
	// total spin
	double S = ((double)siteDim - 1.0) / 2.0 ;

	Tensor<double> Sz(siteDim, siteDim) ;
	Sz = 0 ;

	for (int q1 = 0; q1 < siteDim; q1 ++)
	{
		double m1 = S - (double)q1 ;
		Sz(q1, q1) = m1 ;
	}
//	Sz.display() ;
//	exit(0) ;
	return Sz ;
}

Tensor<double> Operators::createSx()
{
	// total spin
	double S = ((double)siteDim - 1.0) / 2.0 ;

	Tensor<double> Sx(siteDim, siteDim) ;
	Sx = 0 ;

	// Sx = (1/2)*((S+)+(S-))
	for ( int q = 0; q < siteDim; q ++ )
	{
		double m = S - (double)q ;
		// h1 = (1/2)*(S+)
		double h1 = 0.5 * sqrt( S * (S + 1.0) - m * (m + 1.0) ) ;
		if ( h1 != 0 ) // <q-1|h1|q>
		{
			int p = q - 1 ;
			Sx(p, q) = Sx(p, q) + h1 ;
		}
		// h2 = (1/2)*(S-)
		double h2 = 0.5 * sqrt( S * (S + 1.0) - m * (m - 1.0) ) ;
		if ( h2 != 0 ) // <q+1|h1|q>
		{
			int p = q + 1 ;
			Sx(p, q) = Sx(p, q) + h2 ;
		}
	}
	return Sx ;
}

Tensor<double> Operators::createStagSz()
{
	// total spin
	double S = ((double)siteDim - 1.0) / 2.0 ;

	Tensor<double> stagSz(siteDim, siteDim, siteDim, siteDim) ;
	stagSz = 0 ;
	int MM = siteDim * siteDim ;

	// stagSz(m1, m2, n1, n2) = <m1, m2| stagSz | n1, n2 >

	// assign values to diagonal entries
	for ( int q2 = 0; q2 < siteDim; q2 ++)
	{
		double m2 = S - (double)q2 ;
		for ( int q1 = 0; q1 < siteDim; q1 ++)
		{
			double m1 = S - (double)q1 ;
			stagSz(q1, q2, q1, q2) = stagSz(q1, q2, q1, q2) + (m1 - m2) / 2.0 ;
		}
	}

	stagSz = stagSz.reshape(MM, MM) ; //

	return stagSz ;
}

//------------------------------------------------------------------------------------------------------
/*
 *  sigma_z = ( 1,  0 )
 *            ( 0, -1 )
 */
Tensor<double> Operators::createSigma_z()
{
	Tensor<double> sigma_z(2, 2) ;
	sigma_z = 0 ;

	sigma_z(1, 1) = 1.0 ;
	sigma_z(2, 2) = - 1.0 ;

	return sigma_z ;
}

/*
 *  sigma_x = ( 0, 1 )
 *            ( 1, 0 )
 */
Tensor<double> Operators::createSigma_x()
{
	Tensor<double> sigma_x(2, 2) ;
	sigma_x = 0 ;

	sigma_x(1, 2) = 1.0 ;
	sigma_x(2, 1) = 1.0 ;

	return sigma_x ;
}

//----------------------------------------------------------------------------------------------------------------------

TensorArray<double> Operators::createHamiltonian_Kitaev()
{
	cout << "create Kitaev Hamiltonian with field" << endl ;

	TensorArray<double> H(3) ;

	// create AFM Heisenberg interaction
	J = 1.0 ;

	Tensor<double> SzSz = createSzSz() ;
	SzSz = Kz * SzSz ;

	Tensor<double> SxSx = createSxSx() ;
	SxSx = Kx * SxSx ;

	Tensor<double> SySy = createSySy() ;
	SySy = Ky * SySy ;

	Tensor<double> Sz = createSz() ;
	// - Sz
	Tensor<double> mSz = 0.0 - Sz ;

	cout << "Neel field: " << field_z << endl ;
	// Hz = - 2*Kz*S0z*S1z - field*(S0z - S1z)
	H(0) = addField(SzSz, Sz, mSz) ;

	// Hx = - 2*Kx*S0x*S1x - field*(S0z - S1z)
	H(1) = addField(SxSx, Sz, mSz) ;

	// Hy = - 2*Ky*S0y*S1y - field*(S0z - S1z)
	H(2) = addField(SySy, Sz, mSz) ;

	return H ;
}

Tensor<double> Operators::createSzSz()
{
	// total spin
	double S = ((double)siteDim - 1.0) / 2.0 ;

	Tensor<double> H(siteDim, siteDim, siteDim, siteDim) ;
	H = 0 ;
	int MM = siteDim * siteDim ;

	// H(m1, m2, n1, n2) = <m1, m2| S1z*S2z | n1, n2 >
	// = <m1|S1z|n1> * <m2|S2z|n2>

	// assign values to diagonal entries
	for ( int q2 = 0; q2 < siteDim; q2 ++)
	{
		double m2 = S - (double)q2 ;
		for ( int q1 = 0; q1 < siteDim; q1 ++)
		{
			double m1 = S - (double)q1 ;
			H(q1, q2, q1, q2) = H(q1, q2, q1, q2) + (m1 * m2) ;
		}
	}

	H = H.reshape(MM, MM) ; // H((m1, m2), (n1, n2))
//	H.display() ;
//	exit(0) ;
	return H ;
}

Tensor<double> Operators::createSxSx()
{
	// total spin
	double S = ((double)siteDim - 1.0) / 2.0 ;

	Tensor<double> H(siteDim, siteDim, siteDim, siteDim) ;
	H = 0 ;
	int MM = siteDim * siteDim ;

	// H(m1, m2, n1, n2) = <m1, m2| S1x*S2x |n1,n2>
	// = (1/4)<m1,m2|(S1+)(S2+)+(S1+)(S2-)+(S1-)(S2+)+(S1-)(S2-)|n1,n2>

	// assign values to off-diagonal entries
	for ( int q2 = 0; q2 < siteDim; q2 ++)
	{
		double m2 = S - (double)q2 ;
		for ( int q1 = 0; q1 < siteDim; q1 ++)
		{
			double m1 = S - (double)q1 ;

			// h1 = (1/4)(S1+)(S2+)
			double h1 = 0.25 * sqrt((S * (S + 1.0) - m1 * (m1 + 1.0)) * ( S * (S + 1.0) - m2 * (m2 + 1.0) )) ;
			if (h1 != 0) // <(q1-1),(q2-1)|h1|q1,q2>
			{
				int p1 = q1 - 1 ; // raising
				int p2 = q2 - 1 ; // raising
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h1 ;
			}

			// h2 = (1/4)((S1+)(S2-))
			double h2 = 0.25 * sqrt((S * (S + 1.0) - m1 * (m1 + 1.0)) * ( S * (S + 1.0) - m2 * (m2 - 1.0) )) ;
			if ( h2 != 0) // <(q1-1),(q2+1)|h2|q1,q2>
			{
				int p1 = q1 - 1 ; // raising
				int p2 = q2 + 1 ; // lowering
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h2 ;
			}

			// h3 = (1/4)((S1-)(S2+))
			double h3 = 0.25 * sqrt( (S * (S + 1.0) - m1 * (m1 - 1.0)) * ( S * (S + 1.0) - m2 * (m2 + 1.0) ) ) ;
			if ( h3 != 0) // <(q1+1),(q2-1)|h2|q1,q2>
			{
				int p1 = q1 + 1 ;
				int p2 = q2 - 1 ;
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h3 ;
			}

			// h4 = (1/4)(S1-)(S2-)
			double h4 = 0.25 * sqrt( (S * (S + 1.0) - m1 * (m1 - 1.0)) * ( S * (S + 1.0) - m2 * (m2 - 1.0) ) ) ;
			if ( h4 != 0) // <(q1+1),(q2+1)|h2|q1,q2>
			{
				int p1 = q1 + 1 ;
				int p2 = q2 + 1 ;
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h4 ;
			}
		}
	}

	H = H.reshape(MM, MM) ; // H((m1, m2), (n1, n2))
//	H.display() ;
//	exit(0) ;
	return H ;
}

Tensor<double> Operators::createSySy()
{
	// total spin
	double S = ((double)siteDim - 1.0) / 2.0 ;

	Tensor<double> H(siteDim, siteDim, siteDim, siteDim) ;
	H = 0 ;
	int MM = siteDim * siteDim ;

	// H(m1, m2, n1, n2) = <m1, m2| S1y*S2y |n1,n2>
	// = -(1/4)<m1,m2| (S1+)(S2+)-(S1+)(S2-)-(S1-)(S2+)+(S1-)(S2-) |n1,n2>

	// assign values to off-diagonal entries
	for ( int q2 = 0; q2 < siteDim; q2 ++ )
	{
		double m2 = S - (double)q2 ;
		for (int q1 = 0; q1 < siteDim; q1 ++ )
		{
			double m1 = S - (double)q1 ; // 1/2, -1/2

			// h1 = -(1/4)(S1+)(S2+)
			double h1 = - 0.25 * sqrt((S * (S + 1.0) - m1 * (m1 + 1.0)) * ( S * (S + 1.0) - m2 * (m2 + 1.0) )) ;
			if ( h1 != 0 ) // <(q1-1),(q2-1)|h1|q1,q2>
			{
				int p1 = q1 - 1 ; // raising
				int p2 = q2 - 1 ; // raising
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h1 ;
			}

			// h2 = (1/4)((S1+)(S2-))
			double h2 = 0.25 * sqrt((S * (S + 1.0) - m1 * (m1 + 1.0)) * ( S * (S + 1.0) - m2 * (m2 - 1.0) )) ;
			if ( h2 != 0) // <(q1-1),(q2+1)|h2|q1,q2>
			{
				int p1 = q1 - 1 ; // raising
				int p2 = q2 + 1 ; // lowering
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h2 ;
			}

			// h3 = (1/4)((S1-)(S2+))
			double h3 = 0.25 * sqrt( (S * (S + 1.0) - m1 * (m1 - 1.0)) * ( S * (S + 1.0) - m2 * (m2 + 1.0) ) ) ;
			if ( h3 != 0) // <(q1+1),(q2-1)|h2|q1,q2>
			{
				int p1 = q1 + 1 ;
				int p2 = q2 - 1 ;
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h3 ;
			}

			// h4 = -(1/4)(S1-)(S2-)
			double h4 = - 0.25 * sqrt( (S * (S + 1.0) - m1 * (m1 - 1.0)) * ( S * (S + 1.0) - m2 * (m2 - 1.0) ) ) ;
			if ( h4 != 0) // <(q1+1),(q2+1)|h2|q1,q2>
			{
				int p1 = q1 + 1 ;
				int p2 = q2 + 1 ;
				H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h4 ;
			}
		}
	}

	H = H.reshape(MM, MM) ; // H((m1, m2), (n1, n2))
//	H.display() ;
//	exit(0) ;
	return H ;
}

// H12 = - 2 * SzSz - field*(S1z + S2z)
Tensor<double> Operators::addField(Tensor<double> SzSz, Tensor<double> S1z, Tensor<double> S2z)
{
//	S1z.display() ;
//	S2z.display() ;

	Tensor<double> H12 = (- 2.0) * SzSz ;

	H12 = H12.reshape(siteDim, siteDim, siteDim, siteDim) ;

	// H12(p1,p2, q1,q2) = H12(p1,p2, q1,q2) - field*(S1z(p1,q1)*delta(p2,q2) + delta(p1,q1)*S2z(p2,q2))/2
	for (int p1 = 0; p1 < siteDim; p1 ++)
	{
		for (int p2 = 0; p2 < siteDim; p2 ++)
		{
			for (int q1 = 0; q1 < siteDim; q1 ++)
			{
				for (int q2 = 0; q2 < siteDim; q2 ++)
				{
//					cout << p1 << ", " << p2 << ", " << q1 << ", " << q2 << endl ;
//					cout << "H12(p1,p2,q1,q2) = " << H12(p1,p2, q1,q2) << endl ;
					H12(p1,p2, q1,q2) = H12(p1,p2, q1,q2) - field_z * (S1z(p1,q1)*delta(p2,q2) + delta(p1,q1)*S2z(p2,q2)) / 2.0 ;

//					cout << H12(p1,p2, q1,q2) << endl ;
				}
			}
		}

	}

	int MM = siteDim * siteDim ;
	H12 = H12.reshape(MM, MM) ;
//	H12.display() ;
//	exit(0) ;

	return H12 ;
}

// Kronecker delta
double Operators::delta(int a, int b)
{
	if (a == b)
	{
		return 1.0 ;
	}
	else
	{
		return 0.0 ;
	}
}
