/*
 * environment_honeycomb.cpp
 *
 *  Created on: 2014-10-2
 *      Author: ZhaoHuihai
 */

#include <mkl.h>

#include "environment_honeycomb.h"

using namespace std ;

EnvironmentHoneycomb::EnvironmentHoneycomb()
{
	_RGsteps = 0 ;

	RG_bondDim = 0 ;
	minVal = 1 ;

	RG_method = 1 ;
	SRG_trunc = 0 ;
	SRG_norm = false ;
	rho_norm = false ;

	SRG_vari_maxIt = 0 ;
	SRG_vari_tol = 1 ;

	sweep_maxIt = 0 ;
	sweep_tol = 1 ;
}

EnvironmentHoneycomb::EnvironmentHoneycomb(Parameter &parameter)
{
	_RGsteps = parameter.RGsteps ;

	RG_bondDim = parameter.RG_bondDim ;
	minVal = parameter.RG_minVal ;

	RG_method = parameter.RG_method ;
	SRG_trunc = parameter.SRG_trunc ;
	SRG_norm = parameter.SRG_norm ;
	rho_norm = parameter.rho_norm ;

	SRG_vari_maxIt = parameter.SRG_vari_maxIt ;
	SRG_vari_tol = parameter.SRG_vari_tol ;

	sweep_maxIt = parameter.sweep_maxIt ;
	sweep_tol = parameter.sweep_tol ;
}

EnvironmentHoneycomb::EnvironmentHoneycomb(Parameter &parameter, TensorArray<double> &T)
{
	_RGsteps = parameter.RGsteps ;

	RG_bondDim = parameter.RG_bondDim ;
	minVal = parameter.RG_minVal ;

	RG_method = parameter.RG_method ;
	SRG_trunc = parameter.SRG_trunc ;
	SRG_norm = parameter.SRG_norm ;
	rho_norm = parameter.rho_norm ;

	SRG_vari_maxIt = parameter.SRG_vari_maxIt ;
	SRG_vari_tol = parameter.SRG_vari_tol ;

	sweep_maxIt = parameter.sweep_maxIt ;
	sweep_tol = parameter.sweep_tol ;
	//-----------------------------------------------------------------
	// Re(direction[z,x,y], sublattice[0,1], RGsteps)
	Re = TensorArray<double>(3, 2, _RGsteps) ;
	// Te(sublattice[0,1], RGsteps), the original T is included
	Te = TensorArray<double>(2, _RGsteps + 1) ;
	// the original T
	for ( int i = 0 ; i < 2; i ++)
	{
		Te(i, 0) = T(i) ;
	}
	// RGfactor(sublattice[0,1], RGsteps)
	RGfactor = Tensor<double>(2, _RGsteps) ;
	RGfactor = 1 ; // initialization
	//-----------------------------------------------------------------
	TRG() ;
	//-----------------------------------------------------------------
	cout << "+++++++++++++++++++++++++++++++++++++++++++" << endl ;
	if ( RG_method > 1 )
	{
		cout << "In SRG, truncation method is " ;
		if ( SRG_trunc == 1)
		{
			cout << "environment decomposition." << endl ;
		}
		else if ( SRG_trunc == 2 )
		{
			cout << "bond density matrix symmetrization." << endl ;
		}
		else if ( SRG_trunc == 3 )
		{
			cout << "biorthogonal transformation." << endl ;
		}
		else if ( SRG_trunc == 4 )
		{
			cout << "variation." << endl ;
		}
		else if ( SRG_trunc == 5 )
		{
			cout << "isometry variation." << endl ;
		}
		else
		{
			cout << "unknown! " << endl ;
			exit(0) ;
		}
		cout << "+++++++++++++++++++++++++++++++++++++++++++" << endl ;
		// SVs(direction[z,x,y],sublattice[0,1],rgOrder)
		SVs = TensorArray<double>(3, 2, _RGsteps) ;
		SRGfinite() ;
		sweep() ;
	}
}

EnvironmentHoneycomb::EnvironmentHoneycomb(Parameter &parameter, TensorArray<double> &T, bool multiSweep)
{

	_RGsteps = parameter.RGsteps ;

	RG_bondDim = parameter.RG_bondDim ;
	minVal = parameter.RG_minVal ;

	RG_method = parameter.RG_method ;
	SRG_trunc = parameter.SRG_trunc ;
	SRG_norm = parameter.SRG_norm ;
	rho_norm = parameter.rho_norm ;

	SRG_vari_maxIt = parameter.SRG_vari_maxIt ;
	SRG_vari_tol = parameter.SRG_vari_tol ;

	sweep_maxIt = parameter.sweep_maxIt ;
	sweep_tol = parameter.sweep_tol ;
	//-----------------------------------------------------------------
	// Re(direction[z,x,y], sublattice[0,1], RGsteps)
	Re = TensorArray<double>(3, 2, _RGsteps) ;
	// Te(sublattice[0,1], RGsteps), the original T is included
	Te = TensorArray<double>(2, _RGsteps + 1) ;
	// the original T
	for ( int i = 0 ; i < 2; i ++)
	{
		Te(i, 0) = T(i) ;
	}
	// RGfactor(sublattice[0,1], RGsteps)
	RGfactor = Tensor<double>(2, _RGsteps) ;
	RGfactor = 1 ; // initialization
	//-----------------------------------------------------------------
	TRG() ;
	//-----------------------------------------------------------------
	cout << "+++++++++++++++++++++++++++++++++++++++++++" << endl ;
	if ( RG_method > 1 )
	{
		cout << "In SRG, truncation method is " ;
		if ( SRG_trunc == 1)
		{
			cout << "environment decomposition." << endl ;
		}
		else if ( SRG_trunc == 2 )
		{
			cout << "bond density matrix symmetrization." << endl ;
		}
		else if ( SRG_trunc == 3 )
		{
			cout << "biorthogonal transformation." << endl ;
		}
		else if ( SRG_trunc == 4 )
		{
			cout << "variation." << endl ;
		}
		else if ( SRG_trunc == 5 )
		{
			cout << "isometry variation." << endl ;
		}
		else
		{
			cout << "unknown! " << endl ;
			exit(0) ;
		}
		cout << "+++++++++++++++++++++++++++++++++++++++++++" << endl ;
		// SVs(direction[z,x,y],sublattice[0,1],rgOrder)
		SVs = TensorArray<double>(3, 2, _RGsteps) ;
		if (!multiSweep)
		{
			SRGfinite() ;
			sweep() ;
		}
	}
}

EnvironmentHoneycomb::~EnvironmentHoneycomb() { }
//=======================================================================================

// use TRG to find Re and Te
void EnvironmentHoneycomb::TRG()
{
	Tensor<double> truncErr(3) ;

	for ( int j = 0; j < _RGsteps; j ++)
	{
		for (int k = 0; k < 3; k ++) // z, x, y, directions
		{
			renormalizeTtoR(Te(0, j), Te(1, j), Re(k, 0, j), Re(k, 1, j), truncErr(k)) ;

			Te(0, j) = Te(0, j).permute(1, 2, 0) ; // (z,x,y) -> (x,y,z)
			Te(1, j) = Te(1, j).permute(1, 2, 0) ;
		}

		for (int i = 0; i < 2; i ++) // loop for 2 sublattice
		{
			// Rz0,Rx0,Ry0 --> Te1
			RGfactor(i, j) = contract3RtoT(Re(0, i, j), Re(1, i, j), Re(2, i, j), Te(i, j + 1)) ;
		}

		std::cout << "TRG scale " << j << " truncation error: " << truncErr.max() << std::endl ;
	}
}

// RGscale is the initial tensor scale
void EnvironmentHoneycomb::TRG(int RGscale)
{
	for ( int j = RGscale; j < _RGsteps; j ++)
	{
		for ( int k = 0; k < 3; k ++ ) // z, x, y, directions
		{
			renormalizeTtoR(Te(0, j), Te(1, j), Re(k, 0, j), Re(k, 1, j)) ;

			Te(0, j) = Te(0, j).permute(1, 2, 0) ; // (z,x,y) -> (x,y,z)
			Te(1, j) = Te(1, j).permute(1, 2, 0) ;
		}

		double coef ;
		for (int i = 0; i < 2; i ++) // loop for 2 sublattice
		{
			// Rz0,Rx0,Ry0 --> Te1
			coef = contract3RtoT(Re(0, i, j), Re(1, i, j), Re(2, i, j), Te(i, j + 1)) ;
//			Te(i, j + 1) = Te(i, j + 1) * coef ;
		}
	}
}

// RGscale is the initial tensor scale
void EnvironmentHoneycomb::TRG(int startScale, int Nscale)
{
	int RGsteps = min(_RGsteps, (startScale + Nscale)) ;
	for ( int j = startScale; j < RGsteps; j ++)
	{
		for ( int k = 0; k < 3; k ++ ) // z, x, y, directions
		{
			renormalizeTtoR(Te(0, j), Te(1, j), Re(k, 0, j), Re(k, 1, j)) ;

			Te(0, j) = Te(0, j).permute(1, 2, 0) ; // (z,x,y) -> (x,y,z)
			Te(1, j) = Te(1, j).permute(1, 2, 0) ;
		}

		double coef ;
		for (int i = 0; i < 2; i ++) // loop for 2 sublattice
		{
			// Rz0,Rx0,Ry0 --> Te1
			coef = contract3RtoT(Re(0, i, j), Re(1, i, j), Re(2, i, j), Te(i, j + 1)) ;
//			Te(i, j + 1) = Te(i, j + 1) * coef ;
		}
	}
}

//---------------------------------------------------------------------------------

void EnvironmentHoneycomb::renormalizeTtoR(Tensor<double> &T0, Tensor<double> &T1,
																					 Tensor<double> &Rz0, Tensor<double> &Rz1, double& truncErr)
{
	Index z, x0, y0, x1, y1 ;
	// T0(z,x0,y0) T1(z,x1,y1)
	T0.putIndex(z, x0, y0) ;
	T1.putIndex(z, x1, y1) ;
	// M(x0,y0,x1,y1)
	Tensor<double> M = contractTensors(T0, T1) ;
	// M(x0,y0,x1,y1) -> M(x1,y0,x0,y1)
	M = M.permute(2, 1, 0, 3) ;

	int dx1 = M.dimension(0) ;
	int dy0 = M.dimension(1) ;
	int dx0 = M.dimension(2) ;
	int dy1 = M.dimension(3) ;
	// M(x1,y0,x0,y1) -> M((x1,y0),(x0,y1))
	M = M.reshape(dx1 * dy0, dx0 * dy1) ;

	Tensor<double> U, S, V ;
	svd_qr(M, U, S ,V) ; // U((x1,y0),z) V((x0,y1),z)
//	cout << "TRG spectrum: " << endl ;
//	S.display() ;
//	S = S / S(0) ;
	truncateSVD(U, S, V, truncErr) ;

	Tensor<double> sqrtS = S.sqrt() ;

	// U = U((x1,y0),z) * sqrtS(z)
	U = absorbVector(U, 1, sqrtS) ;
	// V = V((x0,y1),z) * sqrtS(z)
	V = absorbVector(V, 1, sqrtS) ;

	U = U.trans() ; // U(z,(x1,y0))
	V = V.trans() ; // V(z,(x0,y1))
	int dz = U.dimension(0) ;

	// Rz0(z,x1,y0)
	Rz0 = U.reshape(dz, dx1, dy0) ;
	// Rz1(z,x0,y1)
	Rz1 = V.reshape(dz, dx0, dy1) ;
}

void EnvironmentHoneycomb::renormalizeTtoR(Tensor<double> &T0, Tensor<double> &T1,
																					 Tensor<double> &Rz0, Tensor<double> &Rz1)
{
	Index z, x0, y0, x1, y1 ;
	// T0(z,x0,y0) T1(z,x1,y1)
	T0.putIndex(z, x0, y0) ;
	T1.putIndex(z, x1, y1) ;
	// M(x0,y0,x1,y1)
	Tensor<double> M = contractTensors(T0, T1) ;
	// M(x0,y0,x1,y1) -> M(x1,y0,x0,y1)
	M = M.permute(2, 1, 0, 3) ;

	int dx1 = M.dimension(0) ;
	int dy0 = M.dimension(1) ;
	int dx0 = M.dimension(2) ;
	int dy1 = M.dimension(3) ;
	// M(x1,y0,x0,y1) -> M((x1,y0),(x0,y1))
	M = M.reshape(dx1 * dy0, dx0 * dy1) ;

	Tensor<double> U, S, V ;
	svd_qr(M, U, S ,V) ; // U((x1,y0),z) V((x0,y1),z)
//	S = S / S(0) ;

	truncateSVD(U, S, V) ;

	Tensor<double> sqrtS = S.sqrt() ;

	// U = U((x1,y0),z) * sqrtS(z)
	U = absorbVector(U, 1, sqrtS) ;
	// V = V((x0,y1),z) * sqrtS(z)
	V = absorbVector(V, 1, sqrtS) ;

	U = U.trans() ; // U(z,(x1,y0))
	V = V.trans() ; // V(z,(x0,y1))
	int dz = U.dimension(0) ;

	// Rz0(z,x1,y0)
	Rz0 = U.reshape(dz, dx1, dy0) ;
	// Rz1(z,x0,y1)
	Rz1 = V.reshape(dz, dx0, dy1) ;
}

void EnvironmentHoneycomb::truncateSVD(Tensor<double> &U, Tensor<double> &S, Tensor<double> &V)
{
	if ( RG_bondDim < S.numel() )
	{
		S = S.subTensor(0, RG_bondDim - 1) ;
		U = U.subTensor(0, U.dimension(0) - 1, 0, RG_bondDim - 1) ;
		V = V.subTensor(0, V.dimension(0) - 1, 0, RG_bondDim - 1) ;
	}
}

void EnvironmentHoneycomb::truncateSVD(Tensor<double> &U, Tensor<double> &S, Tensor<double> &V,
																			 double& truncErr)
{
	int chi = getChi(S) ;

	if ( chi < S.numel() )
	{
		Tensor<double> sqS = S.power(2.0) ; // sqS = S^2
		double sumAll = sqS.sum() ;

		Tensor<double> sqScut = sqS.subTensor(chi, sqS.numel() - 1) ;

		double sumCut = sqScut.sum() ;

		truncErr = sumCut / sumAll ;

		S = S.subTensor(0, chi - 1) ;

		U = U.subTensor(0, U.dimension(0) - 1, 0, chi - 1) ;
		V = V.subTensor(0, V.dimension(0) - 1, 0, chi - 1) ;
	}
	else
	{
		truncErr = 0 ;
	}
}

int EnvironmentHoneycomb::getChi(Tensor<double>& S)
{
	int chi = min(RG_bondDim, S.numel()) ;

//	bool flag = false ;
//	while ( S(chi - 1) < minVal )
//	{
//		chi = chi - 1 ;
//		flag = true ;
//	}
//
//	if (flag)
//	{
//		cout << "singular value is too small" << endl ;
//	}

	return chi ;
}

double EnvironmentHoneycomb::contract3RtoT(Tensor<double> &Rz, Tensor<double> &Rx, Tensor<double> &Ry,
																				 Tensor<double> &T)
{
	// Rz(z,x1,y1) Rx(x,y1,z1) Ry(y,z1,x1)
	Index z, x, y, z1, x1, y1 ;

	Rz.putIndex(z, x1, y1) ;
	Rx.putIndex(x, y1, z1) ;
	Ry.putIndex(y, z1, x1) ;

	// T(z,x1,x,z1) = sum{y1}_[Rz(z,x1,y1) * Rx(x,y1,z1)]
	T = contractTensors(Rz, Rx) ;

	// T(z,x,y) = sum{x1,z1}_[T(z,x1,x,z1) * Ry(y,z1,x1)]
	T = contractTensors(T, Ry) ;

	double coef = 1.0 ;
	// renormalize elements with largest abs value to be 1.0 or - 1.0
	coef = T.maxAbs() ;
	T = T / coef ;
//	cout << "RG factor: " << coef << endl ;
	return coef ;
}

// M(x0,y0,x1,y1)
Tensor<double> EnvironmentHoneycomb::compute_Menv()
{
	// M(xp0,yp0,xp1,yp1)
	Tensor<double> M = compute_initial_env() ;

	for ( int j = _RGsteps; j > 0; j -- )
	{
		lower_scale_env(M, j) ;
	}
	return M ;
}

// M(x0,y0,x1,y1)
Tensor<double> EnvironmentHoneycomb::compute_Menv(int RGscale)
{
	// M(xp0,yp0,xp1,yp1)
	Tensor<double> M = compute_initial_env() ;

	for (int j = _RGsteps; j > RGscale; j -- )
	{
		lower_scale_env(M, j) ;
	}
	return M ;
}

// M(x0,y0,x1,y1)
Tensor<double> EnvironmentHoneycomb::compute_initial_env()
{
	Index a0, a1, a2, a3, a4, a5, a6 ;
	Index x0, y0, x1, y1 ;

	Te(0, _RGsteps).putIndex(a0, x1, a5) ;
	Te(1, _RGsteps).putIndex(a0, x0, a6) ;
	// A(x1,a5,x0,a6) = Te0(a0,x1,a5)*Te1(a0,x0,a6)
	Tensor<double> A = contractTensors(Te(0, _RGsteps), Te(1, _RGsteps)) ;

	Tensor<double> M = A ;
	M.putIndex(a4, a6, a3, a5) ; //

	// M(x1,x0,a4,a3) = A(x1,a5,x0,a6) * M(a4, a6, a3, a5)
	M = contractTensors(A, M) ;

	A.putIndex(a3, y1, a4, y0) ;
	// M(x1,x0,y1,y0) = M(x1,x0,a4,a3) * A(a3, y1, a4, y0)
	M = contractTensors(M, A) ;

	// M(x0,y0,x1,y1)
	M = M.permute(x0, y0, x1, y1) ;

	return M ;
}

// M(x0,y0,x1,y1)
void EnvironmentHoneycomb::lower_scale_env(Tensor<double> &M, int RGscale)
{
	Index x0, y0, x1, y1, xp0, yp0, xp1, yp1 ;
	Index z0, z1 ;

	// Rx0(xp0,y0,z0) Ry0(yp0,z0,x1)
	Re(1, 0, RGscale - 1).putIndex(xp0, y0, z0) ;
	Re(2, 0, RGscale - 1).putIndex(yp0, z0, x1) ;
	// RR(xp0,y0,yp0,x1) = Rx0(xp0,y0,z0) * Ry0(yp0,z0,x1)
	Tensor<double> RR = contractTensors(Re(1, 0, RGscale - 1), Re(2, 0, RGscale - 1)) ;

	M.putIndex(xp0, yp0, xp1, yp1) ;
	// M(y0,x1,xp1,yp1) = RR(xp0,y0,yp0,x1) * M(xp0, yp0, xp1, yp1)
	M = contractTensors(RR, M) ;
	// Rx1(xp1,y1,z1) Ry1(yp1,z1,x0)
	Re(1, 1, RGscale - 1).putIndex(xp1, y1, z1) ;
	Re(2, 1, RGscale - 1).putIndex(yp1, z1, x0) ;
	// RR(xp1,y1,yp1,x0) = Rx1(xp1,y1,z1) * Ry1(yp1,z1,x0)
	RR = contractTensors(Re(1, 1, RGscale - 1), Re(2, 1, RGscale - 1)) ;

	// M(y0,x1,y1,x0) = M(y0,x1,xp1,yp1) * RR(xp1,y1,yp1,x0)
	M = contractTensors(M, RR) ;
	// M(y0,x1,y1,x0) -> M(x0,y0,x1,y1)
	M = M.permute(x0, y0, x1, y1) ;
}

// M(x0,y0,x1,y1)
Tensor<double> EnvironmentHoneycomb::compute_Mz_01(int RGscale)
{
	if ( (_RGsteps - RGscale) == 1 ) // RGscale == 1
	{
		return Mz_01_sites_24() ;
	}
	else if ( (_RGsteps - RGscale) == 2 ) // RGscale == 0
	{
		return Mz_01_sites_72() ;
	}
	else
	{
		std::cout << "compute_Mz_01(int) error: not implemented." << std::endl ;
		exit(0) ;
	}
}

// M(x0,y0,x1,y1)
Tensor<double> EnvironmentHoneycomb::Mz_01_sites_24()
{
	Index a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11 ;
	Index x0, y0, x1, y1, z0, z1 ;

	Te(0, _RGsteps).putIndex(a6, a7, a8) ;
	Te(1, _RGsteps).putIndex(a6, a10, a5) ;
	// TT01(a7,a8,a10,a5) = Te0(a6,a7,a8) * Te1(a6,a10,a5)
	Tensor<double> TT01 = contractTensors(Te(0, _RGsteps), Te(1, _RGsteps)) ;

	Te(2, _RGsteps).putIndex(a3, a4, a5) ;
	Te(3, _RGsteps).putIndex(a3, a1, a8) ;
	// TT23(a4, a5, a1, a8) = Te2(a3, a4, a5) * Te3(a3, a1, a8)
	Tensor<double> TT23 = contractTensors(Te(2, _RGsteps), Te(3, _RGsteps)) ;
	// M(a4,a1,a7,a10) = TT23(a4, a5, a1, a8) * TT01(a7,a8,a10,a5)
	Tensor<double> M = contractTensors(TT23, TT01) ;

	// TT23(a10,a11,a7,a2) = Te2(a9,a10,a11) * Te3(a9,a7,a2)
	TT23.putIndex(a10,a11,a7,a2) ;

	// M(a4,a1,a11,a2) = sum{a7, a10}_[M(a4,a1,a7,a10) * TT23(a10,a11,a7,a2)]
	M = contractTensors(M, TT23) ;

	// Rx0(z0,a1,y0) Ry0(z0,x1,a2)
	Re(1, 0, _RGsteps - 1).putIndex(z0,a1,y0) ;
	Re(2, 0, _RGsteps - 1).putIndex(z0,x1,a2) ;
	// RR(a1,y0,x1,a2) = Rx0(z0,a1,y0) * Ry0(z0,x1,a2)
	Tensor<double> RR = contractTensors(Re(1, 0, _RGsteps - 1), Re(2, 0, _RGsteps - 1)) ;

	// M(y0,x1,a4,a11) = sum{a1,a2}_[RR(a1,y0,x1,a2) * M(a4,a1,a11,a2)]
	M = contractTensors(RR, M) ;

	// Rx1(z1,a4,y1) Ry1(z1,x0,a11)
	Re(1, 1, _RGsteps - 1).putIndex(z1, a4, y1) ;
	Re(2, 1, _RGsteps - 1).putIndex(z1, x0, a11) ;
	// RR(x0,a11,a4,y1) = Ry1(z1,x0,a11) * Rx1(z1,a4,y1)
	RR = contractTensors(Re(2, 1, _RGsteps - 1), Re(1, 1, _RGsteps - 1)) ;

	// M(x0,y1,y0,x1) = RR(x0,a11,a4,y1) * M(y0,x1,a4,a11)
	M = contractTensors(RR, M) ;

	M = M.permute(x0,y0,x1,y1) ;

	return M ;
}

// M(x0,y0,x1,y1)
Tensor<double> EnvironmentHoneycomb::Mz_01_sites_72()
{
	Index z0, z1, x0,y0,x1,y1, xp0,yp0,xp1,yp1 ;
	// M(xp0,yp0,xp1,yp1)
	Tensor<double> M = Mz_01_sites_24() ;
	M.putIndex(xp0, yp0, xp1, yp1) ;

	// Rx0(z0,xp0,y0) Ry0(z0,x1,yp0)
	Re(1, 0, _RGsteps - 2).putIndex(z0, xp0, y0) ;
	Re(2, 0, _RGsteps - 2).putIndex(z0, x1, yp0) ;
	// RR(xp0,y0,x1,yp0) = Rx0(z0,xp0,y0) * Ry0(z0,x1,yp0)
	Tensor<double> RR = contractTensors(Re(1, 0, _RGsteps - 2), Re(2, 0, _RGsteps - 2)) ;

	// M(y0,x1,xp1,yp1) = sum{xp0,yp0}_[RR(xp0,y0,x1,yp0) * M(xp0,yp0,xp1,yp1)]
	M = contractTensors(RR, M) ;

	// Rx1(z1,xp1,y1) Ry1(z1,x0,yp1)
	Re(1, 1, _RGsteps - 2).putIndex(z1, xp1, y1) ;
	Re(2, 1, _RGsteps - 2).putIndex(z1, x0, yp1) ;
	// RR(x0,yp1,xp1,y1) = Ry1(z1,x0,yp1) * Rx1(z1,xp1,y1)
	RR = contractTensors(Re(2, 1, _RGsteps - 2), Re(1, 1, _RGsteps - 2)) ;

	// M(x0,y1,y0,x1) = RR(x0,yp1,xp1,y1) * M(y0,x1,xp1,yp1)
	M = contractTensors(RR, M) ;

	M = M.permute(x0,y0,x1,y1) ;

	return M ;
}
//-----------------------------------------------------
// Menv(x0,y0,x1,y1)
double EnvironmentHoneycomb::computeBond(Parameter& parameter, TensorArray<double> &A,
			  				 TensorArray<double> &T, Tensor<double> &Menv, Tensor<double> H)
{
//	cout << "computeBond_z" << endl ;

	Tensor<double> U, V ;
	// H((m0, m1),(m0', m1')) = sum{n}_(U((m0,m0'),n)*S(n)*V((m1,m1'),n))
	// U((m0,m0'),n) = U((m0,m0'),n)*sqrt(S(n))
	// V((m1,m1'),n) = V((m1,m1'),n)*sqrt(S(n))
	decomposeOperator(parameter, H, U, V) ;

	Tensor<double> Th0, Th1 ;
	// Th0((z,z',n),(x0,x0'),(y0,y0')) = sum{m0,m0'}_[A0(z,x0,y0,m0)*A0(z',x0',y0',m0')*U((m0,m0'),n)]
	Th0 = createTh(A(0), U) ; // change notation: Th0(z,x0,y0)
	// Th1((z,z',n),(x1,x1'),(y1,y1')) = sum{m1,m1'}_[A1(z,x1,y1,m1)*A1(z',x1',y1',m1')*V((m1,m1'),n)]
	Th1 = createTh(A(1), V) ; // change notation: Th1(z,x1,y1)
	//
	Index z, x0, y0, x1, y1 ;
	// Menv(x0,y0,x1,y1)
	Menv.putIndex(x0,y0,x1,y1) ;
	// T0(z,x0,y0)
	T(0).putIndex(z, x0, y0) ;
	Th0.putIndex(z, x0, y0) ;
	// T1(z,x1,y1)
	T(1).putIndex(z, x1, y1) ;
	Th1.putIndex(z, x1, y1) ;
	// W(x0,y0,x1,y1)
	Tensor<double> W = contractTensors(T(0), T(1)) ;
	// W
	W = contractTensors(W, Menv) ;
	double g = W(0) ;

	// Wh(x0,y0,x1,y1)
	Tensor<double> Wh = contractTensors(Th0, Th1) ;
	Wh = contractTensors(Wh, Menv) ;
	double h = Wh(0) ;

	return (h / g) ;
}

double EnvironmentHoneycomb::computeBond(Parameter& parameter, Tensor<double> &Menv,
			TensorArray<double> &T, TensorArray<double> &Th)
{
	Index z, x0, y0, x1, y1 ;
	// Menv(x0,y0,x1,y1)
	Menv.putIndex(x0,y0,x1,y1) ;
	// T0(z,x0,y0)
	T(0).putIndex(z, x0, y0) ;
	Th(0).putIndex(z, x0, y0) ;
	// T1(z,x1,y1)
	T(1).putIndex(z, x1, y1) ;
	Th(1).putIndex(z, x1, y1) ;
	// W(x0,y0,x1,y1)
	Tensor<double> W = contractTensors(T(0), T(1)) ;
	// W
	W = contractTensors(W, Menv) ;
	double g = W(0) ;

	// Wh(x0,y0,x1,y1)
	Tensor<double> Wh = contractTensors(Th(0), Th(1)) ;
	Wh = contractTensors(Wh, Menv) ;
	double h = Wh(0) ;

	return (h / g) ;
}

double EnvironmentHoneycomb::computeSite(Parameter& parameter, TensorArray<double> &A,
																				 TensorArray<double> &T,
																				 Tensor<double> &Menv, Tensor<double> Op0, Tensor<double> Op1)
{
	Tensor<double> Th0, Th1 ;
	// Th0((z,z'),(x0,x0'),(y0,y0')) = sum{m0,m0'}_[A0(z,x0,y0,m0)*A0(z',x0',y0',m0')*Op0(m0,m0')]
	Th0 = createTh_site(A(0), Op0) ;
	// Th1((z,z'),(x1,x1'),(y1,y1')) = sum{m1,m1'}_[A1(z,x1,y1,m1)*A1(z',x1',y1',m1')*Op1(m1,m1')]
	Th1 = createTh_site(A(1), Op1) ;

	Index z, x0, y0, x1, y1 ;
	// Menv(x0,y0,x1,y1)
	Menv.putIndex(x0, y0, x1, y1) ;
	// T0(z,x0,y0)
	T(0).putIndex(z, x0, y0) ;
	Th0.putIndex(z, x0, y0) ;
	// T1(z,x1,y1)
	T(1).putIndex(z, x1, y1) ;
	Th1.putIndex(z, x1, y1) ;
	// W(x0,y0,x1,y1)
	Tensor<double> W = contractTensors(T(0), T(1)) ;
	// W
	W = contractTensors(W, Menv) ;
	double g = W(0) ;

	// Wh(x0,y0,x1,y1)
	Tensor<double> Wh = contractTensors(Th0, Th1) ;
	Wh = contractTensors(Wh, Menv) ;
	double h = Wh(0) ;

	return (h / g) ;
}

// H((m0, m1),(m0', m1')) = sum{n}_(U((m0,m0'),n)*S(n)*V((m1,m1'),n))
// U((m0,m0'),n) = U((m0,m0'),n)*sqrt(S(n))
// V((m1,m1'),n) = V((m1,m1'),n)*sqrt(S(n))
void EnvironmentHoneycomb::decomposeOperator(Parameter& parameter,
		Tensor<double>& H, Tensor<double>& U, Tensor<double>& V)
{
	int d = parameter.siteDim ;

	// H((m0, m1),(m0', m1')) -> H(m0, m1,m0', m1')
	H = H.reshape(d, d, d, d) ;
	// H(m0, m1,m0', m1') -> H(m0,m0',m1,m1')
	H = H.permute(0, 2, 1, 3) ;

	// H(m0,m0',m1,m1') -> H((m0,m0'),(m1,m1'))
	H = H.reshape(d * d, d * d) ;

	Tensor<double> S ;
	// H((m0,m0'),(m1,m1')) = sum{n}_(U((m0,m0'),n)*S(n)*V((m1,m1'),n))
	svd_qr(H, U, S, V) ;
	//***********************************
	// truncate small singular value
	truncateSmall(U, S, V) ;
	//***********************************
	Tensor<double> sqrtS = S.sqrt() ;
	// U((m0,m0'),n) = U((m0,m0'),n)*sqrt(S(n))
	U = absorbVector(U, 1, sqrtS) ;
	// V((m1,m1'),n) = V((m1,m1'),n)*sqrt(S(n))
	V = absorbVector(V, 1, sqrtS) ;
}

void EnvironmentHoneycomb::truncateSmall(
		Tensor<double>& U, Tensor<double>& S, Tensor<double>& V)
{
	int chi = S.numel() - 1 ;

	while ( S(chi) < 1e-12 )
	{
		chi = chi - 1 ;
	}

	S = S.subTensor(0, chi) ;

	U = U.subTensor(0, U.dimension(0) - 1, 0, chi) ;
	V = V.subTensor(0, V.dimension(0) - 1, 0, chi) ;
}


void EnvironmentHoneycomb::truncateSmall(
		Tensor<double>& U, Tensor<double>& S, Tensor<double>& V, double minSing)
{
	int chi = S.numel() - 1 ;
	bool flag = false ;
	while ( S(chi) < minSing )
	{
		flag = true ;
		chi = chi - 1 ;
	}

	if (flag == true)
	{
		cout << "truncate small singular value(s)" << endl ;
	}

	S = S.subTensor(0, chi) ;

	U = U.subTensor(0, U.dimension(0) - 1, 0, chi) ;
	V = V.subTensor(0, V.dimension(0) - 1, 0, chi) ;
}
//---------------------------------------------------------------------------------------------

// Th((z,z',n),(x,x'),(y,y')) = sum{m,m'}_[A(z,x,y,m)*A(z',x',y',m')*U((m,m'),n)]
Tensor<double> EnvironmentHoneycomb::createTh(Tensor<double> &A, Tensor<double> U)
{

	int dz = A.dimension(0) ;
	int dx = A.dimension(1) ;
	int dy = A.dimension(2) ;
	int dm = A.dimension(3) ;

	int dn = U.dimension(1) ;

	// U((m,m'),n) -> U(m,m',n)
	U = U.reshape(dm, dm, dn) ;

	// Th(z,x,y,m',n) = sum{m}_[A(z,x,y,m)*U(m,m',n)]
	Tensor<double> Th = contractTensors(A, 3, U, 0) ;

	// Th(z,x,y,n,z',x',y') = sum{m'}_[Th(z,x,y,m',n) * A(z',x',y',m')]
	Th = contractTensors(Th, 3, A, 3) ;

	// Th(z,x,y,n,z',x',y') -> Th(z,z',n,x,x',y,y')
	Th = Th.permute(0, 4, 3, 1, 5, 2, 6) ;

	// Th(z,z',n,x,x',y,y') -> Th((z,z',n),(x,x'),(y,y'))
	Th = Th.reshape(dz * dz * dn, dx * dx, dy * dy) ;

	return Th ;
}

// Th((z0,z1),(x0,x1),(y0,y1)) = sum{m0,m1}_[A(z0,x0,y0,m0)*A(z1,x1,y1,m1)*h(m0,m1)]
Tensor<double> EnvironmentHoneycomb::createTh_site(Tensor<double> &A, Tensor<double> h)
{
	Index z0, x0, y0, m0, z1, x1, y1, m1 ;

	A.putIndex(z0, x0, y0, m0) ;
	h.putIndex(m0, m1) ;
	// Th(z0,x0,y0,m1) = sum{m0}_[A(z0,x0,y0,m0)*h(m0,m1)]
	Tensor<double> Th = contractTensors(A, h) ;

	A.putIndex(z1, x1, y1, m1) ;
	// Th(z0,x0,y0,z1,x1,y1)
	Th = contractTensors(Th, A) ;

	Th = Th.permute(z0, z1, x0, x1, y0, y1) ;
	return  Th.reshape(Index(z0, z1), Index(x0, x1), Index(y0, y1)) ;
}

//---------------------------------------------------------------------------------
void EnvironmentHoneycomb::SRGfinite()
{
	Tensor<double> allTruncErr(_RGsteps) ;
	Tensor<double> truncErr(3) ;
	TensorArray<double> Menv(3) ;
	for (int i = 0 ; i < _RGsteps; i ++) // loop from lower scale to higher scale
	{
		std::cout << "SRG scale: " << i <<  ", " << std::endl ;
		double t_start = dsecnd() ;
		//**************************************************************************************
//		for (int k = 0; k < 3; k ++) // z, x, y, directions
//		{
//			Tensor<double> Menv = compute_Menv(i) ;
//
//			if ( SRG_trunc != 1 )
//			{
//				SRG_TtoR(i, Te(0, i), Te(1, i), Menv,
//						SVs(k,0,i), SVs(k,1,i), Re(0, 0, i), Re(0, 1, i), truncErr(k)) ;
//			}
//			else
//			{
//				SRG_TtoR_old(i, Te(0, i), Te(1, i), Menv,
//						SVs(k,0,i), SVs(k,1,i), Re(0, 0, i), Re(0, 1, i), truncErr(k)) ;
//			}
//			rotate() ;
//		}
		//**************************************************************************************
		for ( int k = 0; k < 3; k ++ )
		{
			Menv(k) = compute_Menv(i) ;
			rotate() ;
		}
		for ( int k = 0; k < 3; k ++ )
		{
			if ( SRG_trunc != 1 )
			{
				SRG_TtoR(i, Te(0, i), Te(1, i), Menv(k),
						SVs(k,0,i), SVs(k,1,i), Re(0, 0, i), Re(0, 1, i), truncErr(k)) ;
			}
			else
			{
				SRG_TtoR_old(i, Te(0, i), Te(1, i), Menv(k),
						SVs(k,0,i), SVs(k,1,i), Re(0, 0, i), Re(0, 1, i), truncErr(k)) ;
			}
			rotate() ;
		}
		//**************************************************************************************
		for (int j = 0; j < 2; j ++) // loop for 2 sublattice
		{
			// Rz,Rx,Ry --> Te
			RGfactor(j, i) = contract3RtoT(Re(0,j,i), Re(1,j,i), Re(2,j,i), Te(j,i + 1)) ;
		}
		double SRGerr = truncErr.max() ;
		if ( SRGerr > 0.0 ) {
			TRG(i + 1) ;
		}

		allTruncErr(i) = SRGerr ;
		double t_end = dsecnd() ;
		std::cout << "truncation error: " << SRGerr << std::endl ;
		std::cout << "time elapsed: " << (t_end - t_start) << " s" << std::endl ;
	}
	cout << "max truncation error: " <<  allTruncErr.max() << endl ;
	cout << "--------------------------------------------" << endl ;
}

void EnvironmentHoneycomb::sweep()
{
	TensorArray<double> SVs0 = SVs ;

	int step = 0 ;
	double converge = 10.0 ;
	double truncErr ;

	while ( (converge > sweep_tol)  && ( step <  sweep_maxIt) )
	{
		step ++ ;
		std::cout << "SRG sweep step: " << step << std::endl ;
		//====================================================================================
//		for (int i = (_RGsteps - 1) ; i >= 0; i --) // loop from higher scale to lower scale
		for (int i = 0 ; i < _RGsteps; i ++) // loop from lower scale to higher scale
		{
			std::cout << "RG scale: " << i <<  ", " << std::endl ;

			double t_start = dsecnd() ;
			if ( RG_method == 2 ) // finite SRG
			{
				truncErr = SRG(i) ;
//				truncErr = SRGsimple(i) ;
			}
			else if (RG_method == 3) // infinite SRG
			{
				truncErr = SRGinf(i) ;
			}
			double t_end = dsecnd() ;

			std::cout << "truncation error: " << truncErr << std::endl ;
			std::cout << "time elapsed: " << (t_end - t_start) << " s" << std::endl ;
		}
		//====================================================================================
//		if ( step > 1 )
//		{
//			converge = compareVecs_diff(SVs, SVs0) ;
//			cout << "sweep convergence error: " << converge << endl ;
//		}
		SVs0 = SVs ;
	}
	std::cout << "Total SRG sweep steps: " << step << std::endl ;
//	std::cout << "singular value convergence error: " << converge << std::endl ;
}

double EnvironmentHoneycomb::SRG(int RGscale)
{
	Tensor<double> truncErr(3) ;
	//----------------------------------------------------------------------------------------
//	for (int k = 0; k < 3; k ++) // z, x, y, directions
//	{
//		Tensor<double> Menv = compute_Menv(RGscale) ;
//
//		if ( SRG_trunc != 1 )
//		{
//			SRG_TtoR(RGscale, Te(0, RGscale), Te(1, RGscale), Menv,
//					SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, RGscale), Re(0, 1, RGscale), truncErr(k)) ;
//		}
//		else
//		{
//			SRG_TtoR_old(RGscale, Te(0, RGscale), Te(1, RGscale), Menv,
//					SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, RGscale), Re(0, 1, RGscale), truncErr(k)) ;
//		}
//		rotate() ;
//	}
	//****************************************************************************************
	TensorArray<double> Menv(3) ;
	for ( int k = 0; k < 3; k ++ )
	{
		Menv(k) = compute_Menv(RGscale) ;
		rotate() ;
	}
	for ( int k = 0; k < 3; k ++ )
	{
		if ( SRG_trunc != 1 )
		{
			SRG_TtoR(RGscale, Te(0, RGscale), Te(1, RGscale), Menv(k),
					SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, RGscale), Re(0, 1, RGscale), truncErr(k)) ;
		}
		else
		{
			SRG_TtoR_old(RGscale, Te(0, RGscale), Te(1, RGscale), Menv(k),
					SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, RGscale), Re(0, 1, RGscale), truncErr(k)) ;
		}
		rotate() ;
	}
	//----------------------------------------------------------------------------------------
	for (int i = 0; i < 2; i ++) // loop for 2 sublattice
	{
		// Rz,Rx,Ry --> Te
		RGfactor(i, RGscale) = contract3RtoT(Re(0,i,RGscale), Re(1,i,RGscale), Re(2,i,RGscale), Te(i,RGscale + 1)) ;
	}
	//=====================
	double SRGerr = truncErr.max() ;
	if ( SRGerr > 0.0 )
	{
		TRG(RGscale + 1) ;
	}
	//=====================
	return truncErr.max() ;
}

double EnvironmentHoneycomb::SRGsimple(int RGscale)
{
	Tensor<double> truncErr(3) ;
	//----------------------------------------------------------------------------------------
//	for (int k = 0; k < 3; k ++) // z, x, y, directions
//	{
//		Tensor<double> Menv = compute_Menv(RGscale) ;
//
//		if ( SRG_trunc != 1 )
//		{
//			SRG_TtoR(RGscale, Te(0, RGscale), Te(1, RGscale), Menv,
//					SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, RGscale), Re(0, 1, RGscale), truncErr(k)) ;
//		}
//		else
//		{
//			SRG_TtoR_old(RGscale, Te(0, RGscale), Te(1, RGscale), Menv,
//					SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, RGscale), Re(0, 1, RGscale), truncErr(k)) ;
//		}
//		rotate() ;
//	}
	//****************************************************************************************
	TensorArray<double> Menv(3) ;
	for ( int k = 0; k < 3; k ++ )
	{
		Menv(k) = compute_Menv(RGscale) ;
		rotate() ;
	}

	for ( int k = 0; k < 3; k ++ )
	{
		if ( SRG_trunc != 1 )
		{
			SRG_TtoR(RGscale, Te(0, RGscale), Te(1, RGscale), Menv(k),
					SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, RGscale), Re(0, 1, RGscale), truncErr(k)) ;
		}
		else
		{
			SRG_TtoR_old(RGscale, Te(0, RGscale), Te(1, RGscale), Menv(k),
					SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, RGscale), Re(0, 1, RGscale), truncErr(k)) ;
		}
		rotate() ;
	}
	//----------------------------------------------------------------------------------------
	for (int i = 0; i < 2; i ++) // loop for 2 sublattice
	{
		// Rz,Rx,Ry --> Te
		RGfactor(i, RGscale) = contract3RtoT(Re(0,i,RGscale), Re(1,i,RGscale), Re(2,i,RGscale), Te(i,RGscale + 1)) ;
	}
	return truncErr.max() ;
}

double EnvironmentHoneycomb::SRGinf(int RGscale)
{
	Tensor<double> truncErr(3) ;
	TensorArray<double> Menv(3) ;
	for ( int k = 0; k < 3; k ++ )
	{
		Menv(k) = compute_Menv() ;
		rotate() ;
	}

	for ( int k = 0; k < 3; k ++ )
	{
//		SRG_TtoR(RGscale, Te(0, 0), Te(1, 0), Menv(k),
//				SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, 0), Re(0, 1, 0), truncErr(k)) ;
		SRG_TtoR_old(RGscale, Te(0, 0), Te(1, 0), Menv(k),
				SVs(k,0,RGscale), SVs(k,1,RGscale), Re(0, 0, 0), Re(0, 1, 0), truncErr(k)) ;
		rotate() ;
	}
	for (int i = 0; i < 2; i ++) // loop for 2 sublattice
	{
		// Rz,Rx,Ry --> Te
		RGfactor(i, RGscale) = contract3RtoT(Re(0,i,0), Re(1,i,0), Re(2,i,0), Te(i,0)) ;
	}

	if ( RGscale == (_RGsteps - 1) ) // last RG step
	{
		for (int i = 0; i < 2; i ++) // loop for 2 sublattice
		{
			Te(i, _RGsteps) = Te(i, 0) ;
		}
	}
	else
	{
		TRG(0) ;
	}

	return truncErr.max() ;
}

// input: T0, T1, Menv
// output: SVz0, SVz1, Rz0, Rz1, truncErr
void EnvironmentHoneycomb::SRG_TtoR(int RGscale,
		Tensor<double>& T0,   Tensor<double>& T1, Tensor<double>& Menv,
		Tensor<double>& SVz0, Tensor<double>& SVz1,
		Tensor<double>& Rz0,  Tensor<double>& Rz1, double& truncErr)
{
	int dx = T0.dimension(1) ;
	int dy = T0.dimension(2) ;
	if ( RG_bondDim < (dx * dy) )
	{
		Index z, x0, y0, x1, y1 ;
		// T0(z,x0,y0) T1(z,x1,y1)
		T0.putIndex(z, x0, y0) ;
		T1.putIndex(z, x1, y1) ;
		// M(x0,y0,x1,y1)
		Tensor<double> M = contractTensors(T0, T1) ;
		// M(x0,y0,x1,y1) -> M(x1,y0,x0,y1)
		M = M.permute(x1, y0, x0, y1) ;

		// M(x1,y0,x0,y1) -> M((x1,y0),(x0,y1))
		M = M.reshape(Index(x1, y0), Index(x0, y1)) ;

		Tensor<double> U, S, V ;
		svd_qr(M, U, S ,V) ; // U((x1,y0),z) V((x0,y1),z)
		//	S = S / S(0) ;

		Tensor<double> sqrtS = S.sqrt() ;

		// U = U((x1,y0),z) * sqrtS(z)
		U = absorbVector(U, 1, sqrtS) ;
		// V = V((x0,y1),z) * sqrtS(z)
		V = absorbVector(V, 1, sqrtS) ;

		U = U.trans() ; // U(z,(x1,y0))
		V = V.trans() ; // V(z,(x0,y1))

		Index zr(U.dimension(0)), zl(U.dimension(0)) ;

		// Rz0 and Rz1 before truncation
		// Rz0(zr,x1,y0) Rz1(zl,x0,y1)
		Rz0 = U.reshape(zr, x1, y0) ;
		Rz1 = V.reshape(zl, x0, y1) ;

		Menv.putIndex(x0,y0,x1,y1) ;

		// rho(zr,x0,y1) = Rz0(zr,x1,y0) * Menv(x0,y0,x1,y1)
		Tensor<double> rho = contractTensors(Rz0, Menv) ;
		// rho(zr,zl) = rho(zr,x0,y1) * Rz1(zl,x0,y1)
		rho = contractTensors(rho, Rz1) ;

		Tensor<double> PR, PL ;
		//------------------------------------------------
		if ( SRG_trunc == 2 )
		{
			symBondDenMat(RGscale, rho, PR, PL, SVz0, SVz1, truncErr) ;
		}
		else if ( SRG_trunc == 3 )
		{
			biorthogonal(RGscale, rho, PR, PL, SVz0, SVz1, truncErr) ;
		}
		else if ( SRG_trunc == 4 )
		{
			variation(rho, PR, PL, truncErr) ;
		}
		else if ( SRG_trunc == 5 )
		{
			isometry(rho, PR, PL) ; // P = PR Q = PL
		}
		else
		{
			cout << "SRG_TtoR error: unknown truncation method." << endl ;
			exit(0) ;
		}
		//------------------------------------------------
		PR.putIndex(zr, z) ;
		PL.putIndex(zl, z) ;

		// Rz0(z,x1,y0) = PR(zr,z) * Rz0(zr,x1,y0)
		Rz0 = contractTensors(PR, Rz0) ;
		// Rz1(z,x0,y1) = PL(zl,z) * Rz1(zl,x0,y1)
		Rz1 = contractTensors(PL, Rz1) ;
	}
	else
	{
		truncErr = 0.0 ;
	}
}

void EnvironmentHoneycomb::SRG_TtoR_old(int RGscale,
		Tensor<double>& T0,   Tensor<double>& T1, Tensor<double>& Menv,
		Tensor<double>& SVz0, Tensor<double>& SVz1,
		Tensor<double>& Rz0,  Tensor<double>& Rz1, double& truncErr)
{
	int dx = T0.dimension(1) ;
	int dy = T0.dimension(2) ;
	if ( RG_bondDim < (dx * dy) )
	{
		Index z, x0, y0, x1, y1 ;
		// T0(z,x0,y0) T1(z,x1,y1)
		T0.putIndex(z, x0, y0) ;
		T1.putIndex(z, x1, y1) ;
		// M(x0,y0,x1,y1)
		Tensor<double> M = contractTensors(T0, T1) ;
		// M(x0,y0,x1,y1) -> M(x1,y0,x0,y1)
		M = M.permute(x1, y0, x0, y1) ;

		Menv.putIndex(x0,y0,x1,y1) ;
		Menv = Menv.permute(x0,y1,x1,y0) ;
		Menv = Menv.reshape(Index(x0,y1), Index(x1,y0)) ;
		Tensor<double> X, S, Y ;
		svd_qr(Menv, X, S, Y) ; // X((x0,y1),r) Y((x1,y0),l)
		S = S / S(0) ;
//		truncateSmall(X, S, Y, minVal) ; //!!!!
		shift_singVal(S) ;

		Index r(S.numel()), l(S.numel()) ;
		X = X.reshape(x0, y1, r) ;
		Y = Y.reshape(x1, y0, l) ;
		//+++++++++++++++++++++++++++++++++++++++++++
		// M(l,x0,y1) = Y(x1, y0, l) * M(x1,y0,x0,y1)
		M = contractTensors(Y, M) ;
		// M(l,r) = M(l,x0,y1) * X(x0, y1, r)
		M = contractTensors(M, X) ;
	//	cout << "check point" << endl ;
		Tensor<double> sqrtS = S.sqrt() ;
		// M(l,r) = sqrtS(l) * M(l, r)
		M = absorbVector(M, 0, sqrtS) ;
		// M(l,r) = M(l,r) * sqrtS(r)
		M = absorbVector(M, 1, sqrtS) ;
		Tensor<double> U, L, V ;
		svd_qr(M, U, L, V) ; // U(l,z) L(z) V(r,z)
		truncateSVD(U, L, V, truncErr) ;
		SVz0 = L / L(0) ;
		SVz1 = SVz0 ;
		//--------------------------------------------
		double tr_M = 0.0 ;
		double tr_trunc = 0.0 ;
		double coef = 1.0 ;
		tr_M = M.trace() ;
		Tensor<double> T = absorbVector(U, 1, L) ;
		T = contractTensors(T, 1, V, 1) ;
		tr_trunc = T.trace() ;
		truncErr = fabs(1 - tr_trunc / tr_M) ;
//		cout << "coef " << coef << endl ;
		//--------------------------------------------
		if ( SRG_norm == true )
		{
			if (truncErr != 0)
			{
				double tr_M = 0.0 ;
				double tr_trunc = 0.0 ;
				double coef = 1.0 ;

				tr_M = M.trace() ;
				Tensor<double> T = absorbVector(U, 1, L) ;
				T = contractTensors(T, 1, V, 1) ;
				tr_trunc = T.trace() ;
				coef = tr_trunc / tr_M ;
	//			cout << "coef " << coef << endl ;
				if ( !rho_norm )
				{
					// num of sites
					double Nsites = 8.0 * pow(3.0, (double)(_RGsteps - RGscale)) ;
	//				cout << "num of sites: " << Nsites << endl ;
					// num of truncation bonds
					double Nbonds = Nsites / 6.0 ;
					coef = pow(coef, 1.0 / Nbonds) ;
				}
//				cout << "coef every bond " << coef << endl ;
				L = L / coef ;
			}
		}
		//--------------------------------------------
		Tensor<double> sqrtL = L.sqrt() ;
		// U(l,z) = U(l,z) * sqrtL(z)
		U = absorbVector(U, 1, sqrtL) ;
		// V(r,z) = V(r,z) * sqrtL(z)
		V = absorbVector(V, 1, sqrtL) ;

		// U(l,z) = U(l,z) / sqrtS(l)
		U = spitVector(U, 0, sqrtS) ;
		// V(r,z) = V(r,z) / sqrtS(r)
		V = spitVector(V, 0, sqrtS) ;

		U.putIndex(l, z) ;
		V.putIndex(r, z) ;
		// Rz0(z, x1, y0) = U(l,z) * Y(x1, y0, l)
		Rz0 = contractTensors(U, Y) ;
		// Rz1(z, x0, y1) = V(r,z) * X(x0, y1, r)
		Rz1 = contractTensors(V, X) ;
	}
	else
	{
		truncErr = 0.0 ;
	}
}

void EnvironmentHoneycomb::shift_singVal(Tensor<double>& S)
{
//	double minVal = 1e-30 ;

	for (int i = 0; i < S.numel(); i ++)
	{
		if ( S(i) < minVal )
		{
			S(i) = minVal ;
		}
	}
}

void EnvironmentHoneycomb::biorthogonal(int RGscale, Tensor<double>& rho,
		Tensor<double>& PR, Tensor<double>& PL,
		Tensor<double>& SV0, Tensor<double>& SV1, double& truncErr)
{
//	double normFactor = rho.maxAbs() ;
//	rho = rho / normFactor ;
//	rho.display() ;
//	Tensor<double> r1 = rho.trans() ;
//	Tensor<double> rr = r1 - rho ;
//	cout << "sym error: " << rr.maxAbs() << endl ;
//	if ( rr.maxAbs() > 1e-4 )
//	{
//		exit(0) ;
//	}

	Tensor<double> X, S, Y ;
	svd_qr(rho, X, S, Y) ;

	truncateSVD(X, S, Y, truncErr) ;
	// M(b,c) = Y(a,b) * X(a,c)
	Tensor<double> M = contractTensors(Y, 0, X, 0) ;

	Tensor<double> U, L, V ;
	svd_qr(M, U, L, V) ;
	//-----------------------------------------------------------------
//	if ( SRG_norm == true )
//	{
//		if (truncErr != 0)
//		{
//			double tr_rho = 0.0 ;
//			double tr_trunc = 0.0 ;
//			double coef = 1.0 ;
//
//			tr_rho = rho.trace() ;
//			Tensor<double> T = absorbVector(X, 1, S) ;
//			T = contractTensors(T, 1, Y, 1) ;
//			tr_trunc = T.trace() ;
//
//			coef = tr_trunc / tr_rho ;
////			cout << "coef: " << coef << endl ;
//			// num of sites
//			double Nsites = 8 * pow(3.0, (double)(_RGsteps - RGscale)) ;
//			// num of truncation bonds
//			double Nbonds = Nsites / 6.0 ;
////			cout << "coef: " << coef << endl ;
//			coef = pow(coef, 1.0 / Nbonds) ;
////			cout << "coef per bond: " << coef << endl ;
//			L = L / coef ;
//		}
//	}
	//-----------------------------------------------------------------
	S = S / S(0) ;
	SV0 = S ;
	SV1 = S ;

	Tensor<double> sqrtL = L.sqrt() ;
	V = absorbVector(V, 1, sqrtL) ;
	U = absorbVector(U, 1, sqrtL) ;

	PR = contractTensors(X, 1, V, 0) ; // PR = X * V
	PL = contractTensors(Y, 1, U, 0) ; // PL = Y * U
	if ( SRG_norm == true )
	{
		if (truncErr != 0)
		{
			double tr_rho = 0.0 ;
			double tr_trunc = 0.0 ;
			double coef = 1.0 ;
//			rho.info() ;
			tr_rho = rho.trace() ;
			Tensor<double> T = contractTensors(PR, 0, rho, 0) ;
			T = contractTensors(T, 1, PL, 0) ;
			tr_trunc = T.trace() ;
//			T.info() ;
			coef = tr_trunc / tr_rho ;
			// num of sites
			double Nsites = 8 * pow(3.0, (double)(_RGsteps - RGscale)) ;
			// num of truncation bonds
			double Nbonds = Nsites / 6.0 ;
			//	cout << "coef: " << coef << endl ;
			if ( !rho_norm )
			{
				coef = pow(coef, 1.0 / Nbonds) ;
			}
			//			cout << "coef per bond: " << coef << endl ;
			PR = PR / coef ;
		}
	}
	//-----------------------------------------------------------------
}

void EnvironmentHoneycomb::symBondDenMat(int RGscale, Tensor<double>& rho,
		Tensor<double>& PR, Tensor<double>& PL,
		Tensor<double>& SV0, Tensor<double>& SV1, double& truncErr)
{
	rho.symmetrize() ;
	Tensor<double> U, L ;
	symEig(rho, U, L) ; // rho = U * L * U'
	truncateSymEig(U, L, truncErr) ;
	SV0 = L / L.maxAbs() ;
	SV1 = SV0 ;
	//
	if ( SRG_norm == true )
	{
		if (truncErr != 0)
		{
			double tr_rho = 0.0 ;
			double tr_trunc = 0.0 ;
			double coef = 1.0 ;

			tr_rho = rho.trace() ;
			Tensor<double> T = absorbVector(U, 1, L) ;
			T = contractTensors(T, 1, U, 1) ;

			tr_trunc = T.trace() ;

			coef = tr_trunc / tr_rho ;
//			cout << "coef: " << coef << endl ;
			// num of sites
			double Nsites = 8 * pow(3.0, (double)(_RGsteps - RGscale)) ;
			// num of truncation bonds
			double Nbonds = Nsites / 6.0 ;
//			cout << "coef: " << coef << endl ;
			if ( !rho_norm )
			{
				coef = pow(coef, 1.0 / Nbonds) ;
			}
//			cout << "coef per bond: " << coef << endl ;
			U = U / sqrt(coef) ;
		}
	}
	PR = U ;
	PL = U ;
}

void EnvironmentHoneycomb::variation(Tensor<double>& rho,
		Tensor<double>& P, Tensor<double>& Q, double& truncErr)
{
	// initialize P, Q
	Tensor<double> rho_sym = rho ;
	rho_sym.symmetrize() ;
	Tensor<double> W, Lambda ;
	symEig(rho_sym, W, Lambda) ; // rho = W * Lambda * W'
	truncateSymEig(W, Lambda, truncErr) ;
	P = W ;
	Q = W ;
	//-----------------------------------
	Tensor<double> X, Sigma, Y ;
	svd_qr(rho, X, Sigma, Y) ;
	Tensor<double> sqrtSigma = Sigma.sqrt() ;
	Tensor<double> R = absorbVector(X, 1, sqrtSigma) ;
	Tensor<double> L = absorbVector(Y, 1, sqrtSigma) ;
	// RR(r,r') = sum{k}_[R(r,k)*R(r',k)]
	Tensor<double> RR = contractTensors(R, 1, R, 1) ;
	// LL(l,l') = sum{k}_[L(l,k)*L(l',k)]
	Tensor<double> LL = contractTensors(L, 1, L, 1) ;
	//------------------------------------------------------------
	double costFunc;
	double costFunc0 = 100 ;

	int step = 0 ;
	double converge = 10.0 ;
	while ( (converge > SRG_vari_tol) && (step < SRG_vari_maxIt) )
	{
		step ++ ;
		costFunc = optimize_P(RR, LL, P) ;
		costFunc = optimize_Q(RR, LL, P) ;

		converge = fabs((costFunc - costFunc0) / costFunc) ;
//		cout << "costFunc: " << costFunc << endl ;
//		cout << "converge: " << converge << endl ;
		costFunc0 = costFunc ;
	}
	Q = P ;

	cout << "minimizing distance steps: " << step << endl ;
	if ( converge > SRG_vari_tol )
	{
		cout << "variation is not converged, convergence error: " << converge << endl ;
	}
}

double EnvironmentHoneycomb::optimize_P(Tensor<double>& RR, Tensor<double>& LL,
		Tensor<double>& P)
{
	Tensor<double> A = contractTensors(P, 0, LL, 0) ; // A = P' * LL
	Tensor<double> B = A ;

	A = contractTensors(A, 1, P, 0) ; // A = A * P
	A = contractTensors(A, 1, P, 1) ; // A = A * P'
	A = contractTensors(A, 1, RR, 0) ; // A = A * RR

	B = contractTensors(B, 1, RR, 0) ; // B = B * RR
	B = 2.0 * B ;

	Tensor<double> M = A - B ;
	Tensor<double> U, S, V ;
	svd_qr(M, U, S, V) ;
	P = contractTensors(V, 1, U, 1) ; // P = V * U'
	P = 0.0 - P ; // P = -P

	double costFunc =  - S.sum() ;
	return costFunc ;
}

double EnvironmentHoneycomb::optimize_Q(Tensor<double>& RR, Tensor<double>& LL,
		Tensor<double>& Q)
{
	Tensor<double> A = contractTensors(Q, 0, RR, 0) ; // A = Q' * RR
	Tensor<double> B = A ;

	A = contractTensors(A, 1, Q, 0) ; // A = A * Q
	A = contractTensors(A, 1, Q, 1) ; // A = A * Q'
	A = contractTensors(A, 1, LL, 0) ; // A = A * LL

	B = contractTensors(B, 1, LL, 0) ; // B = B * LL
	B = 2.0 * B ;

	Tensor<double> M = A - B ;
	Tensor<double> U, S, V ;
	svd_qr(M, U, S, V) ;
	Q = contractTensors(V, 1, U, 1) ; // Q = V * U'
	Q = 0.0 - Q ; // Q = - Q

	double costFunc = - S.sum() ;
	return costFunc ;
}

// rho(r,l) P(r,z) Q(l,z)
void EnvironmentHoneycomb::isometry(Tensor<double>& rho, Tensor<double>& P, Tensor<double>& Q)
{
	double normFactor = rho.maxAbs() ;
	rho = rho / normFactor ;
//	rho.display() ;
//	Tensor<double> r1 = rho.trans() ;
//	Tensor<double> rr = r1 - rho ;
//	cout << "sym error: " << rr.maxAbs() << endl ;

	Tensor<double> X, Omega, Y ;
//	Tensor<double> A = rho ;
//	A.randUniform(0,1) ;
	svd_qr(rho, X, Omega, Y) ; // rho(r,l)=sum{k}_[X(r,k)*Omega(k)*Y(l,k)]
//	svd_qr(A, X, Omega, Y) ; // rho(r,l)=sum{k}_[X(r,k)*Omega(k)*Y(l,k)]

//	Omega.display() ;
	truncateSVD(X, Omega, Y) ;
	Q = X ;
//	return ;
//	Q.randUniform(0, 1) ;

	double costFunc = 10 ;
	double costFunc0 = 100 ;

	int step = 0 ;
	double converge = 10.0 ;
	while ( (converge > SRG_vari_tol) && (step < SRG_vari_maxIt) )
	{
		step ++ ;

		costFunc = optimize_isoP(rho, P, Q) ;
//		Q = P ;
//		P.display() ;
//		costFunc = optimize_isoQ(rho, P, Q) ;
//		P = Q ;
//		cout << "cost function: " << setprecision(12) << costFunc << endl ;
		converge = fabs((costFunc - costFunc0) / costFunc) ;
//		cout << "converge: " << converge << endl ;
		costFunc0 = costFunc ;
	}
//	Q = P ;
	cout << "cost function: " << setprecision(12) << costFunc << endl ;
	cout << "isometry variation steps: " << step << endl ;
	if ( converge > SRG_vari_tol )
	{
		cout << "isometry variation is not converged, convergence error: " << converge << endl ;
	}
//	if ( rr.maxAbs() > 1e-4 )
//	{
//		P.display() ;
//		exit(0) ;
//	}
}

double EnvironmentHoneycomb::optimize_isoP(Tensor<double>& rho, Tensor<double>& P, Tensor<double>& Q)
{
	// T(r,z) = sum{l}_[rho(r,l)*Q(l,z)]
	Tensor<double> T = contractTensors(rho, 1, Q, 0) ;
	Tensor<double> U, Lambda, V ;
	svd_qr(T, U, Lambda, V) ; // T(r,z)=sum{l}_[U(r,l)*Lambda(l)*V(z,l)]
	// P(r,z) = sum{l}_[U(r,l)*V(z,l)]
	P = contractTensors(U, 1, V, 1) ;
	Q = P ;
//	Lambda.display() ;
	Tensor<double> A = contractTensors(P, 0, rho, 0) ; // A(z,l) = P(r,z)*rho(r,l)
	A = contractTensors(A, 1, Q, 0) ; // A(z,z') = A(z,l) * Q(l,z')

	double costFunc = A.trace() ;
//	cout << "trace: " << costFunc << setprecision(14)  << endl ;
	return costFunc ;
}

double EnvironmentHoneycomb::optimize_isoQ(Tensor<double>& rho, Tensor<double>& P, Tensor<double>& Q)
{
	// T(z,l) = sum{r}_[P(r,z)*rho(r,l)]
	Tensor<double> T = contractTensors(P, 0, rho, 0) ;
	Tensor<double> U, Lambda, V ;
	svd_qr(T, U, Lambda, V) ; // T(z,l)=sum{r}_[U(z,r)*Lambda(r)*V(l,r)]
	// P(l,z) = sum{r}_[V(l,r)*U(z,r)]
	Q = contractTensors(V, 1, U, 1) ;
//	P = Q ;
	double costFunc = Lambda.sum() ;
	return costFunc ;
}

void EnvironmentHoneycomb::truncateSymEig(
		Tensor<double>& U, Tensor<double>& L, double& truncErr)
{
	int chi = std::min(RG_bondDim, L.numel()) ;
//	cout << "chi " << chi << endl ;
	if ( chi < L.numel() ) // need truncation
	{
		Tensor<double> sqL = L.power(2.0) ; // sqL = L^2

		Tensor<double> sqL_decre = sqL.sort('D') ;
//		sqL_decre.display() ;
		double sqLmin = sqL_decre(chi - 1) ;
//		cout << "sqLmin " << sqLmin << endl ;

		while ( sqL_decre.at(chi) >= sqLmin ) // keep degenerate eigen values
		{
			chi ++ ;
//			cout << "chi " << chi << endl ;
			if (chi == L.numel())
			{
				break;
			}
		}

		double sumAll = sqL.sum() ;

		double sumCut = 0 ;
		int j = 0 ;
		Tensor<double> newL(chi), newU(U.dimension(0), chi) ;
		for ( int i = 0; i < L.numel(); i ++ )
		{
			if ( sqL(i) < sqLmin ) // truncate
			{
				sumCut += sqL(i) ;
			}
			else // retain
			{
				newL(j) = L(i) ;

				for (int k = 0; k < U.dimension(0); k ++)
				{
					newU(k, j) = U(k, i) ;
				}
				j ++ ;
			}
		}
//		U.display() ;
//		L.display() ;
		U = newU ;
		L = newL ;

		truncErr = sumCut / sumAll ;
	}
	else
	{
		truncErr = 0 ;
	}
}

double EnvironmentHoneycomb::compareVecs_diff(TensorArray<double>& V, TensorArray<double>& V0)
{
	Tensor<double> Vdiff ;
	Tensor<double> Vnorms(V.numel()) ;

	for(int i = 0 ; i < V.numel(); i ++)
	{
//		std::cout << "i: " << i << std::endl ;
//		V.at(i).display() ;
//		V0.at(i).display() ;
		Vdiff = V.at(i) - V0.at(i) ;
		Vnorms.at(i) = Vdiff.norm('2') ;
	}
	return Vnorms.max() ;
}

void EnvironmentHoneycomb::rotate()
{
	for ( int j = 0 ; j < (_RGsteps + 1); j ++) // loop for RG scale
	{
		Te(0, j) = Te(0, j).permute(1, 2, 0) ; // (z,x,y) -> (x,y,z)
		Te(1, j) = Te(1, j).permute(1, 2, 0) ;
	}

	for ( int j = 0 ; j < _RGsteps; j ++)
	{
		for (int sublat = 0; sublat < 2; sublat ++)
		{
			Tensor<double> Rz = Re(0, sublat, j) ;
			Tensor<double> Rx = Re(1, sublat, j) ;
			Tensor<double> Ry = Re(2, sublat, j) ;

			Re(0, sublat, j) = Rx ;
			Re(1, sublat, j) = Ry ;
			Re(2, sublat, j) = Rz ;
		}
	}
}

//-----------------------------------------------------------------------------------------------

double EnvironmentHoneycomb::freeEnergy()
{
	double lnZ = 0.0 ; // log of partition function
	for ( int j = 0; j < _RGsteps; j ++)
	{
		double count = 4.0 * pow((double)3, ( _RGsteps - j - 1) ) ;

		for ( int i = 0; i < 2; i ++) // loop for 2 sublattice
		{
			cout << "RGfactor(" << i << ", " << j << " ): " << setprecision(15) << RGfactor(i, j) << endl ;
			lnZ += count * log( RGfactor(i, j) ) ;
		}
	}
	//------------------------------------------------
	Index a0, a1, a2, a4, a5, a8, a11 ;

	Te(0, _RGsteps).putIndex(a0, a1, a2 ) ;
	Te(1, _RGsteps).putIndex(a0, a4, a11) ;
	// A(a1,a2,a4,a11) = Te0(a0,a1,a2) * Te1(a0,a4,a11)
	Tensor<double> A = contractTensors(Te(0, _RGsteps), Te(1, _RGsteps)) ;

	Tensor<double> M = A ;
	M.putIndex(a4, a5, a1, a8) ;
	// M(a2,a11,a5,a8) = A(a1,a2,a4,a11) * M(a4, a5, a1, a8)
	M = contractTensors(A, M) ;

	A = M ;
	A.putIndex(a11, a2, a8, a5) ;

	M = contractTensors(M, A) ;
	if ( M(0) > 0 )
	{
		cout << "trace out coef: " << M(0) << endl ;
		lnZ += log( M(0) ) ;
	}
	else
	{
		std::cout << "freeEnergy error: negative partition function!" << std::endl ;
		exit(0) ;
	}
	//-------------------------------------------------
	return lnZ ;
}


