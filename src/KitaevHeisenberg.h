/*
 * KitaevHeisenberg.h
 *
 *  Created on: 2014骞�1���23���
 *      Author: ZhaoHuihai
 */

#ifndef KITAEVHEISENBERG_H_
#define KITAEVHEISENBERG_H_

#include <string>

#include "environment_honeycomb.h"

class KitaevHeisenberg
{
private:
public:
	// constructor
	KitaevHeisenberg() ;
	KitaevHeisenberg(Parameter& parameter) ;
	// destructor
	~KitaevHeisenberg() ;
	//===============================
	void findExpectValue(Parameter& parameter, WaveFunction &wave) ;
	double findEnergy(Parameter& parameter, WaveFunction &wave) ;

	double computeMag(Tensor<double> MzSite, Tensor<double> MxSite) ;

	Tensor<double> tracePhysInd(Tensor<double> &A) ;

	double computeSite(Parameter& parameter, WaveFunction &wave,
			EnvironmentHoneycomb &env, TensorArray<double> &T, Tensor<double> Op1, Tensor<double> Op2) ;
	Tensor<double> createTh_site(Tensor<double> &A, Tensor<double> h) ;

	double averageBond(Tensor<double> bond) ;
	double standard_deviation(Tensor<double>& bond) ;

	void displayEnergyBond(Tensor<double> energyBond) ;

};



#endif /* KITAEVHEISENBERG_H_ */
