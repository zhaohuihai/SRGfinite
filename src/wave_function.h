/*
 * wave_function.h
 *
 *  Created on: 2011-6-21
 *      Author: zhaohuihai
 */

#ifndef WAVE_FUNCTION_H_
#define WAVE_FUNCTION_H_

#include <string>

#include "TensorOperation.hpp"
#include "parameter.h"

class WaveFunction
{
public:
	// constructor
	WaveFunction() ;
	WaveFunction(Parameter &parameter) ;
	WaveFunction(std::string dirName) ;
	// destructor
	~WaveFunction() ;
	//--------------------------
	void createNewWave(Parameter &parameter) ;

	void initializeInfo(Parameter &parameter) ;
	void createDirName(Parameter &parameter) ;

	Tensor<double> initializeSite(Parameter &parameter) ;
	Tensor<double> initializeBond(Parameter &parameter) ;
	//----------------------
	TensorArray<double>  A ;
	// Lambda.rank() = A.rank() + 1 ;
	TensorArray<double> Lambda ;

	// tensor order
	int tensOrd ; // 3 for honeycomb 4 for square
	// number of sublattice
	int subLatNo ;

	// dimension of physical index
	int siteDim ;
	// bond dimension of wave function
	int bondDim ;

	bool polarization ;
	// simple update steps
	int simpleStep ;
	// full variation update steps
	int fullStep ;
	// full gradient update steps
	int gradientStep ;

	double tau ;

	std::string dirName ;
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	void info() ;
	void save() ;
	bool load() ;
	//*********************manipulation******************************************************
	void rotate() ; //

};

#endif /* WAVE_FUNCTION_H_ */
